This library provides various classes allowing to connect to a LDAP directory and to a XiVO, via the restapi.
It provides deserialization from LDAP or XiVO entries to User objects.
It is also able to perform the update and the creation of users on XiVO.
It can be used in "Mock" mode, which just writes the changes to a file instead of sending them to XiVO or LDAP.

# Configuration

Les fichiers de configuration suivants doivent se trouver dans le répertoire config/ :

- logging.properties
- general.ini
- config.ini

## logging.properties

Fichier de configuration des logs. CF. la documentation officielle de Java.

## general.ini

Décrit les URL d'accès à confd

## config.ini

Configure le connecteur LDAP.

### Section FieldMapping

Configure les liens entre les attributs LDAP et les champs du XiVO.

### Section XiVO

- XiVOIP : Adresse IP du XiVO.
- restapiPort : Port de l'API Rest.
- context : Context par défaut.
- reenableLiveReload : Reactive le rechargement à chaud en fin d'opération.
- installationId: Une chaine de caractères qui identifie le connecteur XiVO <-> LDAP. Peut être utilisée 
pour par exemple mettre à jour un champ du LDAP.

Exemple : 
```java
    public class UserModifierImpl implements UserModifier {

        public User modifyLdapUser(User user) {
            String installationId = ConfigLoader.getConfig().installationId();
    
            user.setTimezone(installationId);
```

Any String which can be used to uniquely identified the connector and therefore the xivo.
This string can be used 

### Section Actions

Actions à effectuer sur les objets Utilisateur, Ligne, Boîte vocale et Appel entrant.
C = Create, U = Update, D = Delete
updatedLdapFields = attributs à mettre à jour côté LDAP

### Section LDAP

- ldapSynchronizationFilter : filtre à appliquer sur les entrées du LDAP
- ldapUrl : URL du LDAP
- ldapLogin : login de connexion au LDAP
- ldapPassword : mot de passe
- ldapRoot : DN de base pour la recherche des utilisateurs
- useTls : activation ou non du TLS (true/false) ; voir détails de la configuration ci-dessous
- followReferrals : suivre ou non les referrals (true/false : en général true est une valeur appropriée)

# Codes d'erreur:

Sur des erreurs importantes la lib termine l'éxécution du programme pour éviter de causer des dégats.
Les codes d'erreur utilisés sont les suivants (cf. ExitCodes.java) :
```
    LDAP_ACCESS_ERROR(1),
    CONFIGURATION_ERROR(2),
    UNREADABLE_USERFIELD(3),
    LOG_CONFIGURATION_ERROR(4),
    WRONG_CLI_PARAMETERS(5),
    REST_API_ERROR(6),
    CODE_STRUCTURE_ERROR(7);
```

# Support du TLS

1. Deux Solutions (fonctionnent dans des cas différents mais pas bien identifiés):
    1. soit dans le fichier `config/config.ini`, positionner le paramètre useTls à true:
       ```
       useTls=true
       ```
    1. soit repmplacer `ldap://` par `ldaps://` dans l'URI du LDAP
1. ajouter le certificat SSL du serveur LDAP aux certificats de la jvm :
    ```
    cd /usr/lib/jvm/java-6-openjdk-amd64/jre/lib/security
    sudo keytool -importcert -alias equifaxsecureca -keystore cacerts  -file $PATH_TO_PEM_FILE
    ```
    (le mot de passe, si non modifié, est "changeit")

