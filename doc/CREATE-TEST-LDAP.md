# Create LDAP server in docker

`docker run --name ldaptest --network=isolated_nw --ip=10.50.0.111 -e LDAP_ORGANISATION="XiVO Test Corp" -e LDAP_DOMAIN="xivo-dev.com" -e LDAP_ADMIN_PASSWORD="superpass" -d osixia/openldap:1.3.0`

# Create objects in ldap

Push reference object to ldap server:

`docker cp departments.ldif ldaptest:/tmp`
`docker cp sales-members.ldif ldaptest:/tmp`

Enter a shell inside the ldap server container:

`docker exec -it ldaptest bash`

Inside, run the following commands 

```
alias ldapadd='/usr/bin/ldapadd -x -H ldap://localhost -D "cn=admin,dc=xivo-dev,dc=com" -w superpass'

ldapadd -f /tmp/departments.ldif
ldapadd -f /tmp/sales-members.ldif

alias ldapsearch='/usr/bin/ldapsearch -x -H ldap://localhost -D "cn=admin,dc=xivo-dev,dc=com" -w superpass'

ldapsearch -b "ou=sales,dc=xivo-dev,dc=com"
```

# Sample ini files for synchronization project

``` general.ini
;NE PAS MODIFIER CE FICHIER

[General]
reportFolder=./log/xivo-ldap-synchronizer

[Restapi]
configmgtUrl=http://dev-xivo:9100/configmgt
configmgtToken=u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k
queuesUrl=/api/1.0/queues
agentGroupsUrl=/api/1.0/groups
restapiUrl=https://dev-xivo:9486
voicemailsUrl=/1.1/voicemails
usersUrl=/1.1/users
agentsUrl=/1.1/agents
membershipsUrl=/1.1/queues
sipLinesUrl=/1.1/lines_sip
linesUrl=/1.1/lines
extensionsUrl=/1.1/extensions
userLinksUrl=/1.1/user_links
ctiProfilesUrl=/1.1/cti_profiles
configurationUrl=/1.1/configuration
devicesUrl=/1.1/devices
sipEndpointsUrl=/1.1/endpoints/sip
externalContextName=from-extern
```

``` config.ini
[FieldMapping]
User.email=mail
User.username=uid
User.password=uid
User.firstname=givenName
User.lastname=sn
User.mobile_phone_number=mobile
User.caller_id=cn
User.outgoing_caller_id=ipPhone
IncomingCall.sda=telephoneNumber
Line.number=homePhone
Agent.number=homePhone
Agent.firstname=givenName
Agent.lastname=sn
Agent.groupname=ou
Agent.queuename=ou
User.userfield=ou
Voicemail.email=mail
Voicemail.name=cn
Voicemail.number=homePhone
; l'objectGUID est de type Hexadecimal, on ne peut pas le requêter 
; il faut le laisser commenter, une adaptation est faite dans le code pour le récupérer
; for the record . JBM
;User.description=uid


[XiVO]
XiVOIP=dev-xivo
restapiPort=9487
context=default
reenableLiveReload=true
installationId=jpt


[Actions]
User=C,U
Line=C,U
IncomingCall=C,U
;Voicemail=C,U,D
Agent=C,U
;updatedLdapFields=telephoneNumber,ipPhone,mobile

[LDAP]
#ldapUrl=ldap://10.1.1.13
ldapUrl=ldap://ldaptest
ldapLogin=cn=admin,dc=xivo-dev,dc=com
ldapPassword=superpass
ldapRoot=ou=sales,dc=xivo-dev,dc=com
ldapSynchronizationFilter=(objectClass=inetOrgPerson)
```
