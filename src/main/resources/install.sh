#!/bin/bash
if [ "$#" -ne 1 ]; then
  echo "Usage: $0 user" >&2
  exit 1
fi

user=$1;
report_folder=`grep "reportFolder" config/general.ini | cut -d = -f 2`

if [ "$(lsb_release -rs)" -gt 10 ] 
then
  ## NEW IN DEBIAN 10 
  apt update
  apt install -y apt-transport-https wget
  mkdir -p /etc/apt/keyrings
  wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public | tee /etc/apt/keyrings/adoptium.asc
  echo "deb [signed-by=/etc/apt/keyrings/adoptium.asc] https://packages.adoptium.net/artifactory/deb $(awk -F= '/^VERSION_CODENAME/{print$2}' /etc/os-release) main" | tee /etc/apt/sources.list.d/adoptium.list
  apt update
  apt install -y temurin-8-jdk --fix-missing
  mkdir $report_folder;
  adduser --disabled-password --gecos "" --force-badname $user
  chown -R $user $report_folder;
  chown -R $user .
  adduser $user asterisk
else 
  ## For Debian < 10 
  apt-get update
  apt-get install openjdk-7-jre
  mkdir $report_folder;
  chown -R $user $report_folder;
  chown -R $user .
  adduser $user asterisk
fi 

echo "Installation finished. Please check the user $user has asterisk in its path.";
