package xivo.utils;

import org.apache.commons.lang.StringUtils;

public class PropertyTester {
    public interface Accessor<T> {
        String getProperty(T a);
    }

    public static <T> boolean propertyNeedsUpdate(T a, T b, Accessor<T> accessor) {
        if (a == null && b == null) {
            return false;
        }
        if (a == null && b != null) {
            return true;
        }

        if (a != null && b == null) {
            return true;
        }

        String aVal = accessor.getProperty(a);
        String bVal = accessor.getProperty(b);
        return !StringUtils.equals(aVal, bVal);
    }

    public static <T> boolean propertiesNeedsUpdate(T a, T b, Accessor<T> ...accessors) {
        if (a == null && b == null) {
            return false;
        }
        if (a == null && b != null) {
            return true;
        }

        if (a != null && b == null) {
            return true;
        }

        for (Accessor<T> accessor: accessors) {
            String aVal = accessor.getProperty(a);
            String bVal = accessor.getProperty(b);
            if(!StringUtils.equals(aVal, bVal)) {
                return true;
            }
        }
        
        return false;
    }
}
