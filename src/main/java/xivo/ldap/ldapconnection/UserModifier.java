package xivo.ldap.ldapconnection;

import javax.naming.NamingException;
import javax.naming.directory.SearchResult;

import xivo.restapi.model.User;

public interface UserModifier {
    public User modifyXivoUser(User user, SearchResult searchResult) throws NamingException;
    public User modifyLdapUser(User user);
}
