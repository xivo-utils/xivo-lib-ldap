package xivo.ldap.ldapconnection;

import java.util.Set;
import java.util.logging.Logger;

import com.google.inject.Inject;

import xivo.ldap.UserIntrospector;
import xivo.restapi.model.User;

public class UpdateRequestFactory {

    private Logger logger = Logger.getLogger(getClass().getName());

    @Inject(optional = true)
    protected UserIdentifier userIdentifier;
    private FieldMapping mapping;

    @Inject
    public UpdateRequestFactory(FieldMapping mapping) {
        this.mapping = mapping;
    }

    public UpdateRequest createUpdateRequest(User user, Set<String> updatedLdapFields)
            throws NoSuchFieldException, IllegalAccessException {
        String searchCriteria = userIdentifier.getSearchCriteria(user);
        UpdateRequest request = new UpdateRequest(searchCriteria);
        for (String field : updatedLdapFields) {
            String xivoField = null;
            if (mapping.getXivoFields(field) != null) {
                xivoField = mapping.getXivoFields(field).iterator().next();
            } else {
                throw new NoSuchFieldException(field);
            }
            Object fieldValue = UserIntrospector.getUserField(user, xivoField);
            if (fieldValue != null && !fieldValue.toString().equals("")) {
                logger.info("Updating field " + field + " = " + fieldValue +  " (based on xivo field " + xivoField + ")");
                request.addLdapField(field, fieldValue.toString());
            }
        }
        return request;
    }
}
