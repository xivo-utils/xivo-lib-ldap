package xivo.ldap.ldapconnection;

import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.CommunicationException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import xivo.ldap.configuration.ExitCodes;
import xivo.restapi.model.User;

public class LDAPConnectorImpl implements LDAPConnector {

    protected LdapContext context;
    private Logger logger = Logger.getLogger(getClass().getName());
    private String ldapRoot;
    private LDAPDeserializer ldapDeserializer;
    private UpdateRequestFactory updateRequestFactory;
    private String searchCriteria;
    private ContextFactory contextFactory;
    private UserModifier userModifier;

    @Inject
    public LDAPConnectorImpl(ContextFactory contextFactory, @Named("ldapRoot") String ldapRoot,
            LDAPDeserializer ldapDeserializer, UpdateRequestFactory updateRqfactory,
            @Named("ldapSynchronizationFilter") String ldapSynchronizationFilter, UserModifier userModifier) {
        logger.fine("New LDAP Connector created");
        this.ldapRoot = ldapRoot;
        this.ldapDeserializer = ldapDeserializer;
        this.contextFactory = contextFactory;
        searchCriteria = ldapSynchronizationFilter;
        updateRequestFactory = updateRqfactory;
        this.userModifier = userModifier;
        connect();
    }

    protected void connect() {
        context = contextFactory.get();
    }

    public void setContext(LdapContext context) {
        this.context = context;
    }

    public void setUpdateRequestFactory(UpdateRequestFactory factory) {
        updateRequestFactory = factory;
    }

    public void setUserModifier(UserModifier modifier) {
        userModifier = modifier;
    }

    private NamingEnumeration<SearchResult> requestLDAP(String searchCriteria) throws NamingException {
        int nbTries = 0;
        SearchControls constraints = new SearchControls();
        constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
        constraints.setCountLimit(0);
        while (nbTries < 2) {
            try {
                return context.search(this.ldapRoot, searchCriteria, constraints);
            } catch (CommunicationException e) {
                nbTries++;
                logger.info("Connection to the LDAP reset, trying to reconnect...");
                connect();
            }
        }
        logger.severe("Impossible to reconnect to the LDAP, exiting");
        System.exit(ExitCodes.LDAP_ACCESS_ERROR.ordinal());
        return null;
    }

    @Override
    public void updateUsers(List<User> users, Set<String> updatedFields) {
        for (User user : users) {
            User updatedUser = userModifier.modifyLdapUser(user);
            try {
                UpdateRequest request = updateRequestFactory.createUpdateRequest(updatedUser, updatedFields);
                String dn = getDN(request.searchCriteria);
                ModificationItem[] items = new ModificationItem[request.getLdapFields().size()];
                int i = 0;
                for (Entry<String, String> entry : request.getLdapFields()) {
                    Attribute attribute = new BasicAttribute(entry.getKey(), entry.getValue());
                    items[i] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attribute);
                    i++;
                }
                context.modifyAttributes(dn, items);
            } catch (NoSuchFieldException e) {
                logger.log(Level.SEVERE, "One of the updated fields does not exist, stopping.", e);
                return;
            } catch (IllegalAccessException e) {
                logger.log(Level.SEVERE, "Error accessing one of the fields, stopping", e);
                return;
            } catch (NamingException e) {
                logger.log(Level.WARNING, "Error updating the LDAP for user " + updatedUser, e);
            } catch (UserNotFoundException e) {
                logger.log(Level.WARNING, "The user was not found.", e);
            }
        }
    }

    private String getDN(String searchCriteria) throws UserNotFoundException, NamingException {
        NamingEnumeration<SearchResult> searchResults = requestLDAP(searchCriteria);
        if (searchResults.hasMoreElements()) {
            return searchResults.nextElement().getNameInNamespace();
        } else {
            throw new UserNotFoundException(searchCriteria);
        }
    }

    @Override
    public List<User> getUsers() throws NamingException {
        logger.info("Retrieving all users: " + this.ldapRoot + ", search criteria: " + searchCriteria);
        NamingEnumeration<SearchResult> searchResults = requestLDAP(searchCriteria);
        List<User> users = ldapDeserializer.createAllUsers(searchResults);
        logger.finest("Users from LDAP: " + users.toString());
        return users;
    }
}
