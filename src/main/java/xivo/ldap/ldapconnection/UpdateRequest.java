package xivo.ldap.ldapconnection;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class UpdateRequest {

    protected String searchCriteria;
    private Map<String, String> ldapFields = new HashMap<String, String>();

    public UpdateRequest(String searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public Set<Entry<String, String>> getLdapFields() {
        return ldapFields.entrySet();
    }

    public void addLdapField(String fieldName, String fieldValue) {
        ldapFields.put(fieldName, fieldValue);
    }

}
