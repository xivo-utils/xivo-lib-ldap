package xivo.ldap.ldapconnection;

import xivo.restapi.model.User;

public interface UserIdentifier {
    public String getSearchCriteria(User user);
}
