package xivo.ldap.ldapconnection;

public class UserNotFoundException extends Exception {

    private static final long serialVersionUID = 5021139345304841410L;

    public UserNotFoundException(String searchCriteria) {
        super("Could not find the user: " + searchCriteria);
    }

}
