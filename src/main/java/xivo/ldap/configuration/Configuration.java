package xivo.ldap.configuration;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import xivo.ldap.ldapconnection.FieldMapping;
import xivo.restapi.connection.RestapiConfig;

public class Configuration implements RestapiConfig {

    protected FieldMapping fieldMapping;
    protected Actions users;
    protected Actions voicemails;
    protected Actions lines;
    protected Actions incalls;
    protected Actions agents;
    protected Set<String> updatedLdapFields;
    protected Map<String, String> params;

    public boolean createAgents() {
        return agents.create;
    }
    
    public boolean createUsers() {
        return users.create;
    }

    public boolean createLines() {
        return lines.create;
    }

    public boolean createVoicemails() {
        return voicemails.create;
    }

    public boolean updateAgents() {
        return agents.update;
    }
    
    public boolean updateUsers() {
        return users.update;
    }

    public boolean updateLines() {
        return lines.update;
    }

    public boolean updateVoicemails() {
        return voicemails.update;
    }

    public boolean deleteAgents() {
        return agents.delete;
    }

    public boolean deleteUsers() {
        return users.delete;
    }

    public boolean deleteLines() {
        return lines.delete;
    }

    public boolean deleteVoicemails() {
        return voicemails.delete;
    }

    public boolean updateLdap() {
        return updatedLdapFields != null && updatedLdapFields.size() > 0;
    }

    public Set<String> getUpdatedLdapFields() {
        return updatedLdapFields;
    }

    public boolean deleteIncalls() {
        return incalls.delete;
    }

    public boolean updateIncalls() {
        return incalls.update;
    }

    public boolean createIncalls() {
        return incalls.create;
    }

    public String get(String parameter) {
        return params.get(parameter);
    }

    public FieldMapping getFieldMapping() {
        return fieldMapping;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Set<String> getMasteredFields() {
        FieldMapping mapping = this.getFieldMapping();
        Set<String> masteredFields = new HashSet<String>();
        for (String ldapField : this.getUpdatedLdapFields()) {
            Set<String> fields = mapping.getXivoFields(ldapField);
            if (fields != null) {
                masteredFields.addAll(fields);
            }
        }
        return masteredFields;
    }

    public boolean useTls() {
        return params.get("useTls") != null && params.get("useTls").equals("true");
    }

    public Boolean followReferrals() {
        return params.get("followReferrals") != null && params.get("followReferrals").equals("true");
    }

    public boolean mustReenableLiveReload() {
        return params.get("reenableLiveReload") != null && params.get("reenableLiveReload").equals("true");
    }

    public String installationId() {
        return params.get("installationId") != null ? params.get("installationId") : "";
    }

}
