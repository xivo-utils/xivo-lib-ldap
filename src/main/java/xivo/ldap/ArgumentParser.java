package xivo.ldap;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class ArgumentParser {

    public static final String DRYRUN = "dryrun";
    public static final String INITIATE = "initiate";
    public static final String HELP = "help";

    private CommandLine command;

    public ArgumentParser() {

    }

    private Options generateOptions() {
        Options options = new Options();
        options.addOption("d", DRYRUN, false, "Changes are written to a file instead of being executed.");
        options.addOption("i", INITIATE, false, "Initiates the XiVO database to allow starting synchronization");
        options.addOption("h", HELP, false, "Prints this help message.");
        return options;
    }

    public ParsingResult parse(String[] arguments) throws ParseException {
        CommandLineParser parser = new BasicParser();
        Options options = generateOptions();
        command = parser.parse(options, arguments);
        if (command.hasOption(HELP)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar LDAPSynchronizer.jar", options);
            System.exit(0);
        }
        return new ParsingResult(command);
    }

}
