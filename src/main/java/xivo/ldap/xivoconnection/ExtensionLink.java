package xivo.ldap.xivoconnection;

public class ExtensionLink {
    private int extensionId;
    private String number;

    public ExtensionLink(int extensionId, String number) {
        this.extensionId = extensionId;
        this.number = number;
    }

    public int getExtensionId() {
        return extensionId;
    }

    public String getNumber() {
        return number;
    }
}
