package xivo.ldap.xivoconnection;

import com.google.inject.Inject;
import xivo.restapi.model.Context;

import java.sql.SQLException;
import java.util.List;

public class NumberGeneratorFactory {
    private PostgresConnector postgres;

    @Inject
    public NumberGeneratorFactory(PostgresConnector postgres) {
        this.postgres = postgres;
    }

    public NumberGenerator createLineNumberGenerator(String contextName) throws EmptyContextException, SQLException {
        Context context = postgres.getContext(contextName, "user");
        List<String> existingNums = postgres.getExtensionsInContext(contextName);
        return new NumberGenerator(context, existingNums);
    }

    public NumberGenerator createSdaGenerator(String contextName) throws EmptyContextException, SQLException {
        Context context = postgres.getContext(contextName, "incall");
        List<String> existingNums = postgres.getExtensionsInContext(contextName);
        return new NumberGenerator(context, existingNums);
    }
}
