package xivo.ldap.xivoconnection;

import java.sql.SQLException;

import xivo.restapi.connection.DefaultRemoteRestApiConfig;
import xivo.restapi.connection.RestapiConfig;
import xivo.restapi.connection.WebServicesConnectorFactory;

public class XivoConnectorFactory {

    private String xivoHost;
    private WebServicesConnectorFactory wsFactory;

    public XivoConnectorFactory(String xivoHost, RestapiConfig restapiConfig) {
        this.xivoHost = xivoHost;
        wsFactory = new WebServicesConnectorFactory(restapiConfig);
    }
    public XivoConnectorFactory(String xivoHost) {
        this(xivoHost, new DefaultRemoteRestApiConfig(xivoHost));
    }

    public RealXivoConnector getRealXivoConnector() throws SQLException {
        return new RealXivoConnector(wsFactory.get(), new PostgresConnector(new PostgresConnectionFactory(xivoHost)));
    }
}
