package xivo.ldap.xivoconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

public class PostgresConnectionFactory implements Provider<Connection> {

    public static String dbName = "asterisk";
    public static String user = "asterisk";
    public static String password = "proformatique";

    private String host;
    private Connection connection;
    private Logger logger = Logger.getLogger(getClass().getName());

    public PostgresConnectionFactory() throws SQLException {
        host = "localhost";
        connection = createNewConnection();
    }

    @Inject
    public PostgresConnectionFactory( @Named("XiVOIP") String host) throws SQLException {
        this.host = host;
        connection = createNewConnection();
    }

    private Connection createNewConnection() throws SQLException {
        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", password);
        props.setProperty("stringtype", "unspecified");
        logger.info("Creating new connection to db host : " + this.host + " user : " + user);
        return DriverManager.getConnection("jdbc:postgresql://" + host + "/" + dbName, props);
    }

    @Override
    public Connection get() {
        try {
            connection.createStatement().execute("SELECT 1");
            return connection;
        } catch (SQLException e) {
            try {
                connection = createNewConnection();
                return connection;
            } catch (SQLException e1) {
                logger.log(Level.SEVERE, "Cannot connect to the database " + dbName + " on host " + host, e1);
                throw new RuntimeException(e1);
            }
        }
    }

    @Override
    public void finalize() {
        try {
            connection.close();
        } catch (SQLException e) {
            logger.log(Level.WARNING,
                    "SQL Exception while closing the connection. Not a big deal but should be investigated.", e);
        }
    }

}
