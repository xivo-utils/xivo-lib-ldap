package xivo.ldap.xivoconnection;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.*;
import xivo.restapi.model.Links.UserVoicemail;

public interface XivoConnector {

    public static final String DEFAULT_CTI_PROFILE = "Client";

    public User createUser(User user) throws IOException, WebServicesException;

    public List<User> getAllUsers() throws IOException, WebServicesException, SQLException;

    public void updateUser(User user) throws IOException, WebServicesException;
    public void updateUser(User user, Boolean useDefaults) throws IOException, WebServicesException;

    public IncomingCall getIncomingCallForUser(User user) throws IOException;

    public ExtensionLink getInternalNumberForUser(String context, int userId) throws IOException;

    public List<String> getFreeSdas();

    public Info getXivoInfo() throws IOException, WebServicesException;

    public List<Mds> listMds() throws IOException, WebServicesException;

    public void associateIncallToUser(User user) throws IOException, SQLException;

    public void createAgentForUser(User user) throws IOException, WebServicesException;
    
    public void createLineForUser(User user) throws IOException, WebServicesException;

    public void createVoicemailForUser(User user) throws IOException, WebServicesException;

    public void associateVoicemailToUser(User user) throws IOException, WebServicesException;

    public void dissociateVoicemailFromUser(User user) throws IOException, WebServicesException;

    public void updateAgentForUser(User user) throws IOException, WebServicesException;
    
    public void updateLineForUser(User user) throws IOException, WebServicesException;

    public void updateVoicemailForUser(User user) throws IOException, WebServicesException;

    public void deleteUser(User user) throws IOException, WebServicesException;

    public void deleteAgentForUser(User user) throws IOException, WebServicesException;

    public void deleteLineForUser(User user) throws IOException, WebServicesException;

    public void deleteVoicemailForUser(User user) throws IOException, WebServicesException;

    public void deleteIncallForUser(User user) throws IOException, SQLException;

    public void updateIncallForUser(User user) throws IOException, SQLException, EmptyContextException,
            NumberOutOfContextException;

    public void createIncallForUser(User user) throws IOException, EmptyContextException, NumberOutOfContextException,
            SQLException;

    public CtiProfile getDefaultCtiProfile() throws IOException;

    public void enableLiveReload() throws IOException;

    public void disableLiveReload() throws IOException;

    public User getUser(int userId) throws IOException, WebServicesException;

    public void createContext(Context context) throws IOException, SQLException;

    public int getUsersNumber() throws IOException, WebServicesException;

    public void createContextInclusion(String context, String includedContext) throws IOException, SQLException;

    public void createContextInclusion(String context, String includedContext, int priority) throws IOException,
            SQLException;

    public void createTrunk(Trunk trunk) throws IOException, SQLException;

    public boolean extensionExists(String exten, String context) throws SQLException;

    public void createWebServicesUser(String login, String password, String address) throws SQLException;

    public void createWebServicesUserWithAcl(String login, String password, String address, String acl)
            throws SQLException;

    public List<String> getExtensionsInContext(String context) throws SQLException;

    public void deleteContext(String context) throws SQLException;

    public void deleteContextInclusion(String context, String includedContext) throws SQLException;

    public boolean contextExists(String context) throws SQLException;

    public List<Context> listUserContexts() throws SQLException;

    public boolean trunkExists(String trunk) throws SQLException;

    public void updateContext(Context context) throws SQLException;

    public void synchronizeDevice(Device device) throws IOException, WebServicesException;

    public void resetAutoprov(Device device) throws IOException, WebServicesException;

    public List<UserVoicemail> getUsersIdsAssociatedToVoicemail(int voicemailId) throws WebServicesException, IOException;

    public List<Voicemail> listVoicemails() throws IOException, WebServicesException;
}
