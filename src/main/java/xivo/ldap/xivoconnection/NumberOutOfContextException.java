package xivo.ldap.xivoconnection;

public class NumberOutOfContextException extends Exception {

    private static final long serialVersionUID = 4467146030325107960L;

    private String number;
    private String context;

    public NumberOutOfContextException(String number, String context) {
        this.number = number;
        this.context = context;
    }

    public String getNumber() {
        return number;
    }

    public String getContext() {
        return context;
    }

}
