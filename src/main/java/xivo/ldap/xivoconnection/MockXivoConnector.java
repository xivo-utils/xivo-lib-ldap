package xivo.ldap.xivoconnection;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.*;
import xivo.restapi.model.Links.UserVoicemail;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class MockXivoConnector implements XivoConnector {

    public static final String MOCK_XIVO_UPDATE_FILE = "mock_xivo_update.log";

    private RealXivoConnector realConnector;
    private Logger logger;
    private File logFile;

    @Inject
    public MockXivoConnector(RealXivoConnector realConnector, @Named("reportFolder") String reportFolder) {
        this.realConnector = realConnector;
        this.logger = Logger.getLogger(getClass().getName());
        File reportDirectory = new File(reportFolder);
        if (!reportDirectory.exists()) {
            reportDirectory.mkdir();
        }
        logFile = new File(reportFolder + MOCK_XIVO_UPDATE_FILE);
        if (logFile.exists()) {
            logFile.delete();
        }
        logger.info("Updates to XiVO will be written to " + reportFolder + MOCK_XIVO_UPDATE_FILE);
    }

    private void logAction(String prefix, List<?> objects) throws IOException {
        List<String> lines = new ArrayList<String>();
        for (Object object : objects) {
            lines.add(prefix + object.toString());
        }
        FileUtils.writeLines(logFile, "UTF-8", lines, true);
    }

    @Override
    public List<User> getAllUsers() throws IOException, WebServicesException, SQLException {
        return realConnector.getAllUsers();
    }

    @Override
    public void updateUser(User user) throws IOException {
        logAction("User update: ", Arrays.asList(new User[] { user }));
    }

    @Override
    public void updateUser(User user, Boolean useDefaults) throws IOException {
        logAction("User update without default (null) values: ", Arrays.asList(new User[] { user }));
    }


    @Override
    public IncomingCall getIncomingCallForUser(User user) throws IOException {
        return realConnector.getIncomingCallForUser(user);
    }

    @Override
    public ExtensionLink getInternalNumberForUser(String context, int userId) throws IOException {
        return null;
    }

    @Override
    public List<String> getFreeSdas() {
        return realConnector.getFreeSdas();
    }

    @Override
    public synchronized Info getXivoInfo() throws IOException, WebServicesException {
        return realConnector.getXivoInfo();
    }

    @Override
    public List<Mds> listMds() throws IOException, WebServicesException {
        return realConnector.listMds();
    }

    @Override
    public void associateIncallToUser(User user) throws IOException {
        List<String> strings = new ArrayList<String>();
        String line = user.getFullname() + " has SDA " + user.getIncomingCall().getSda();
        strings.add(line);
        FileUtils.writeLines(logFile, strings, true);
    }

    @Override
    public void createAgentForUser(User user) throws IOException {
        logAction("Agent create: ", Arrays.asList(new User[] { user }));
    }

    @Override
    public void createLineForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        if (user.getLine() != null) {
            String line = user.getLine().toString();
            lines.add(line);
        }
        logAction("Line creation: ", lines);
        List<String> strings = new ArrayList<String>();
        if (user.getLine() != null) {
            String line = "user " + user.getFirstname() + " " + user.getLastname() + " has the line "
                    + user.getLine().getNumber();
            strings.add(line);
        }
        logAction("User - line association : ", strings);
    }

    @Override
    public void createVoicemailForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line = user.getFullname() + " has the voicemail " + user.getVoicemail();
        lines.add(line);
        logAction("Voicemail creation: ", lines);
    }

    @Override
    public void associateVoicemailToUser(User user) throws IOException, WebServicesException {
    }

    @Override
    public void dissociateVoicemailFromUser(User user) throws IOException, WebServicesException {
    }

    @Override
    public void updateAgentForUser(User user) throws IOException {
        logAction("Agent update: ", Arrays.asList(new User[] { user }));
    }

    @Override
    public void updateLineForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        if(user.getLine() != null)
            lines.add(user.getFullname() + " has the line " + user.getLine().getNumber());
        logAction("Line update: ", lines);
    }

    @Override
    public void updateVoicemailForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line = user.getFullname() + " has the voicemail " + user.getVoicemail();
        lines.add(line);
        logAction("Voicemail update: ", lines);
    }

    @Override
    public void deleteUser(User user) throws IOException {
        logAction("User deletion: ", Arrays.asList(new User[] { user }));
    }

    @Override
    public void deleteAgentForUser(User user) throws IOException {
        logAction("Agent delete: ", Arrays.asList(new User[] { user }));
    }

    @Override
    public void deleteLineForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        if (user.getLine() != null) {
            String line = "line number " + user.getLine().getNumber() + " for user " + user.getFullname();
            lines.add(line);
        }
        logAction("Line deletion: ", lines);
    }

    @Override
    public void deleteVoicemailForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        String str = "voicemail " + user.getVoicemail() + " for user " + user.getFullname();
        lines.add(str);
        logAction("Voicemail deletion: ", lines);
    }

    @Override
    public void deleteIncallForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        if (user.getIncomingCall() != null) {
            String str = "incoming call " + user.getIncomingCall().getSda() + " for user " + user.getFullname();
            lines.add(str);
        }
        logAction("Incoming call deletion: ", lines);
    }

    @Override
    public void updateIncallForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        if (user.getIncomingCall() != null) {
            String str = "incoming call " + user.getIncomingCall().getSda() + " for user " + user.getFullname();
            lines.add(str);
        }
        logAction("Incoming call update: ", lines);
    }

    @Override
    public void createIncallForUser(User user) throws IOException {
        List<String> lines = new ArrayList<String>();
        if (user.getIncomingCall() != null) {
            String str = "incoming call " + user.getIncomingCall().getSda() + " for user " + user.getFullname();
            lines.add(str);
        }
        logAction("Incoming call creation: ", lines);
    }

    @Override
    public CtiProfile getDefaultCtiProfile() throws IOException {
        return realConnector.getDefaultCtiProfile();
    }

    @Override
    public void enableLiveReload() {
        logger.info("Enabling live relaod in dry-run mode : nothing to do !");
    }

    @Override
    public void disableLiveReload() {
        logger.info("Disabling live relaod in dry-run mode : nothing to do !");
    }

    @Override
    public User getUser(int userId) throws IOException, WebServicesException {
        return realConnector.getUser(userId);
    }

    @Override
    public void createContext(Context context) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public int getUsersNumber() throws IOException, WebServicesException {
        return realConnector.getUsersNumber();
    }

    @Override
    public void createContextInclusion(String context, String includedContext) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void createTrunk(Trunk trunk) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public User createUser(User user) throws IOException, WebServicesException {
        logAction("User creation: ", Arrays.asList(new User[] { user }));
        return user;
    }

    @Override
    public boolean extensionExists(String exten, String context) throws SQLException {
        return realConnector.extensionExists(exten, context);
    }

    @Override
    public void createWebServicesUser(String login, String password, String address) {
        // TODO Auto-generated method stub
    }

    @Override
    public void createWebServicesUserWithAcl(String login, String password, String address, String acl) {
    }

    @Override
    public List<String> getExtensionsInContext(String context) throws SQLException {
        return realConnector.getExtensionsInContext(context);
    }

    @Override
    public void deleteContext(String context) throws SQLException {
        // TODO Auto-generated method stub
    }

    @Override
    public void createContextInclusion(String context, String includedContext, int priority) throws IOException,
            SQLException {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteContextInclusion(String context, String includedContext) throws SQLException {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean contextExists(String context) throws SQLException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<Context> listUserContexts() throws SQLException {
        return Collections.<Context> emptyList();
    }

    @Override
    public boolean trunkExists(String trunk) throws SQLException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void updateContext(Context context) throws SQLException {
        // TODO Auto-generated method stub
    }

    @Override
    public void synchronizeDevice(Device device) throws IOException, WebServicesException {
    }

    @Override
    public void resetAutoprov(Device device) throws IOException, WebServicesException {
    }

    @Override
    public List<Links.UserVoicemail> getUsersIdsAssociatedToVoicemail(int voicemailId) throws WebServicesException, IOException {
        return new ArrayList<>();
    }

    @Override
    public List<Voicemail> listVoicemails() throws WebServicesException, IOException {
        return new ArrayList<>();
    }

}
