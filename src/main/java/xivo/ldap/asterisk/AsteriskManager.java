package xivo.ldap.asterisk;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;

public class AsteriskManager {

    private Logger logger = Logger.getLogger(getClass().getName());

    public void reloadVoicemails() {
        logger.info("Reloading voicemails.");
        try {
            executeCommand(new String[] { "/usr/sbin/asterisk", "-rx", "voicemail reload" });
        } catch (CommandException e) {
            logger.log(Level.SEVERE, "Voicemail reload failed.", e);
        }
    }

    protected void executeCommand(String[] command) throws CommandException {
        Process process;
        int status = 0;
        try {
            process = Runtime.getRuntime().exec(command);
            List<String> result = IOUtils.readLines(process.getInputStream());
            logger.fine("Command result : " + result);
            status = process.waitFor();
        } catch (Exception e) {
            throw new CommandException(e.getMessage());
        }
        if (status != 0)
            throw new CommandException("The command \"" + command + "\" exited with non-zero status " + status);
    }

    public void reloadDialplan() {
        logger.info("Reloading dialplan.");
        try {
            executeCommand(new String[] { "/usr/sbin/asterisk", "-rx", "dialplan reload" });
        } catch (CommandException e) {
            logger.log(Level.SEVERE, "Dialplan reload failed.", e);
        }
    }

    public void reloadCore() {
        logger.info("Reloading asterisk");
        try {
            executeCommand(new String[] { "/usr/sbin/asterisk", "-rx", "core reload" });
        } catch (CommandException e) {
            logger.log(Level.SEVERE, "Core reload failed.", e);
        }

    }

}
