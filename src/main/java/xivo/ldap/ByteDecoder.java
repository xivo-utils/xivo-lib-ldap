package xivo.ldap;

public class ByteDecoder {

    public String getADHexString(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String transformed = prefixZeros((int) bytes[i] & 0xFF);
            result.append("\\");
            result.append(transformed);
        }
        return result.toString();
    }

    private static String prefixZeros(int value) {
        if (value <= 0xF) {
            return "0" + Integer.toHexString(value);
        } else {
            return Integer.toHexString(value);
        }
    }
}
