package xivo.restapi.connection;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class HttpClientFactory implements Provider<DefaultHttpClient> {

    private Logger logger;
    private RestapiConfig config;

    @Inject
    public HttpClientFactory(RestapiConfig config) {
        logger = Logger.getLogger(this.getClass().getName());
        this.config = config;
    }

    public DefaultHttpClient get() {
        logger.info("Creating the HTTP client");
        DefaultHttpClient client = new DefaultHttpClient();
        URL url = null;
        try {
            url = new URL(config.get(RestapiConfig.RESTAPI_URL));
        } catch (MalformedURLException e) {
            logger.severe("The Rest API URL is malformed : " + config.get(RestapiConfig.RESTAPI_URL));
            System.exit(1);
        }
        if (needsHttps(url)) {
            TrustStrategy easyStrategy = createTrustStrategy();
            SSLSocketFactory sf = null;
            try {
                sf = new SSLSocketFactory(easyStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Cannot create the SSL socket", e);
                return null;
            }
            client.getConnectionManager().getSchemeRegistry().register(new Scheme("https", url.getPort(), sf));
        }
        return client;
    }

    private TrustStrategy createTrustStrategy() {
        return new TrustStrategy() {
            public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                // TODO : something more restrictive
                return true;
            }
        };
    }

    private boolean needsHttps(URL url) {
        return url.getProtocol().equals("https");
    }

}
