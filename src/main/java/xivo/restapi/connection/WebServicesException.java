package xivo.restapi.connection;

public class WebServicesException extends Exception {

    private static final long serialVersionUID = 7020163353444877763L;

    private int status;

    public WebServicesException(String message, int status) {
        super(message);
        this.setStatus(status);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.getMessage() + ": " + this.status;
    }
}
