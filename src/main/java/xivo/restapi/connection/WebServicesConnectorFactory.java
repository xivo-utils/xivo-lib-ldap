package xivo.restapi.connection;

import org.apache.http.impl.client.DefaultHttpClient;

import com.google.inject.Provider;

public class WebServicesConnectorFactory implements Provider<WebServicesConnector> {
    private RestapiConfig config;
    private HttpClientFactory clientFactory;

    public WebServicesConnectorFactory(RestapiConfig config) {
        this.config = config;
        clientFactory = new HttpClientFactory(config);
    }

    public WebServicesConnector get() {
        JsonSerializer serializer = new JsonSerializer();
        DefaultHttpClient client = clientFactory.get();
        RequestExecutor executor = new RequestExecutor(client);
        return new WebServicesConnector(executor, serializer, config);
    }

}
