package xivo.restapi.connection.managers;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

import com.google.gson.reflect.TypeToken;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.Links.UserVoicemail;
import xivo.restapi.model.Voicemail;

public class VoicemailManager extends Manager {

    private String voicemailsUrl;
    private Logger logger;

    public VoicemailManager(RequestExecutor executor, JsonSerializer serializer, String voicemailsUrl) {
        super(executor, serializer);
        this.voicemailsUrl = voicemailsUrl;
        logger = Logger.getLogger(getClass().getName());
    }

    public void delete(Voicemail voicemail) throws IOException, WebServicesException {
        HttpDelete delete = new HttpDelete(voicemailsUrl + "/" + voicemail.getId());
        try {
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - voicemail deletion : " + e.getMessage(), e.getStatus());
        }
    }

    public void create(Voicemail voicemail) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(voicemailsUrl);
        String returnedValue = null;
        try {
            returnedValue = executor.fillAndExecuteRequest(post, serializer.serializePost(voicemail));
        } catch (RequestException e) {
            throw new WebServicesException("POST - create voicemail : " + e.getMessage(), e.getStatus());
        }
        if (!returnedValue.isEmpty()) {
            int id = serializer.extractId(returnedValue);
            voicemail.setId(id);
        }
    }

    public void update(Voicemail voicemail) throws IOException, WebServicesException {
        HttpPut put = new HttpPut(voicemailsUrl + "/" + voicemail.getId());
        try {
            executor.fillAndExecuteRequest(put, serializer.serializePut(voicemail));
        } catch (RequestException e) {
            throw new WebServicesException("PUT - update voicemail : " + e.getMessage(), e.getStatus());
        }
    }

    public Voicemail get(int id) throws IOException, WebServicesException {
        HttpGet get = new HttpGet(voicemailsUrl + "/" + id);
        String result;
        try {
            result = executor.executeRequest(get);
        } catch (RequestException e) {
            throw new WebServicesException("GET - get voicemail : " + e.getMessage(), e.getStatus());
        }
        return serializer.deserializeObject(result, Voicemail.class);
    }

    public List<UserVoicemail> getAssociatedUsersIds(int id) throws IOException, WebServicesException {
        HttpGet get = new HttpGet(voicemailsUrl + "/" + id + "/users");
        String result;
        try {
            result = executor.executeRequest(get);
        } catch (RequestException e) {
            throw new WebServicesException("GET - get voicemail : " + e.getMessage(), e.getStatus());
        }
        return serializer.deserializeList(result, new TypeToken<JsonSerializer.ResponseContent<UserVoicemail>>() {
        });
    }

    public List<Voicemail> list() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(voicemailsUrl);
        String result;
        try {
            result = executor.executeRequest(get);
        } catch (RequestException e) {
            throw new WebServicesException("GET - list voicemails : " + e.getMessage(), e.getStatus());
        }
        return serializer.deserializeList(result, new TypeToken<JsonSerializer.ResponseContent<Voicemail>>() {
        });
    }
}
