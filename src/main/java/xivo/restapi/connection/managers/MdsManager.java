package xivo.restapi.connection.managers;

import org.apache.http.client.methods.HttpGet;
import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.Mds;

import java.io.IOException;
import java.util.List;

public class MdsManager extends Manager {

    private String mdsUrl;
    private String token;

    public MdsManager(RequestExecutor executor, JsonSerializer serializer, String mdsUrl, String token) {
        super(executor, serializer);
        this.mdsUrl = mdsUrl;
        this.token = token;
    }

    public List<Mds> list() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(mdsUrl);
        get.setHeader("X-Auth-Token", token);
        try {
            String result = executor.executeRequest(get);
            return serializer.deserializeMds(result);
        } catch (RequestException e) {
            throw new WebServicesException("GET - list mediaservers : " + e.getMessage(), e.getStatus());
        }
    }
}
