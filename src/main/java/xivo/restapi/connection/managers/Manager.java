package xivo.restapi.connection.managers;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestExecutor;

public abstract class Manager {

    protected RequestExecutor executor;
    protected JsonSerializer serializer;

    protected Manager(RequestExecutor executor, JsonSerializer serializer) {
        this.executor = executor;
        this.serializer = serializer;
    }

}
