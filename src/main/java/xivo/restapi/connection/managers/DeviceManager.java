package xivo.restapi.connection.managers;

import java.io.IOException;

import org.apache.http.client.methods.HttpGet;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.Device;

public class DeviceManager extends Manager {

    private String devicesUrl;

    public DeviceManager(RequestExecutor executor, JsonSerializer serializer, String devicesUrl) {
        super(executor, serializer);
        this.devicesUrl = devicesUrl;
    }

    public void synchronize(Device device) throws IOException, WebServicesException {
        HttpGet get = new HttpGet(devicesUrl + "/" + device.getId() + "/synchronize");
        try {
            executor.executeRequest(get);
        } catch (RequestException e) {
            throw new WebServicesException("GET - synchronize : " + e.getMessage(), e.getStatus());
        }
    }

    public void resetAutoprov(Device device) throws IOException, WebServicesException {
        HttpGet get = new HttpGet(devicesUrl + "/" + device.getId() + "/autoprov");
        try {
            executor.executeRequest(get);
        } catch (RequestException e) {
            throw new WebServicesException("GET - reset autoprov : " + e.getMessage(), e.getStatus());
        }
    }

}
