package xivo.restapi.connection.managers;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.JsonSerializer.ResponseContent;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.Agent;

import com.google.gson.reflect.TypeToken;
import xivo.restapi.model.AgentGroup;
import xivo.restapi.model.Queue;
import xivo.restapi.model.QueueMembership;

public class AgentManager extends Manager {

    private String agentsUrl;
    private String queuesUrl;
    private String configmgtToken;
    private String membershipsUrl;
    private String agentGroupsUrl;
    private List<Queue> _queuesCache = null;
    private List<AgentGroup> _groupsCache = null;
    private Logger logger;

    public AgentManager(RequestExecutor executor, JsonSerializer serializer, String agentsUrl, String queuesUrl, String configmgtToken, String membershipsUrl, String agentGroupsUrl) {
        super(executor, serializer);
        this.agentsUrl = agentsUrl;
        this.queuesUrl = queuesUrl;
        this.configmgtToken = configmgtToken;
        this.membershipsUrl = membershipsUrl;
        this.agentGroupsUrl = agentGroupsUrl;
        logger = Logger.getLogger(getClass().getName());
    }

    public Agent get(int id) throws WebServicesException, IOException {
        HttpGet get = new HttpGet(agentsUrl + "/" + id);
        try {
            String result = executor.executeRequest(get);
            Agent agent = serializer.deserializeObject(result, Agent.class);
            agent.initMemberships(getAgentMembership(id));
            agent.setGroupname(getGroupById(agent.getNumgroup()).getName());
            return agent;
        } catch (RequestException e) {
            throw new WebServicesException("GET - get agent : " + e.getMessage(), e.getStatus());
        }
    }

    public void update(Agent agent) throws IOException, WebServicesException {
        if(StringUtils.isEmpty(agent.getNumber())) {
            throw new WebServicesException("Cannot create agent without number " + agent.toString(), 500);
        }
        verifyAgentGroupName(agent);
        HttpPut put = new HttpPut(agentsUrl + "/" + agent.getId());
        try {
            executor.fillAndExecuteRequest(put, serializer.serializePut(agent));
            saveAgentMembership(agent);
        } catch (RequestException e) {
            throw new WebServicesException("update agent: " + e.getMessage(), e.getStatus());
        }
    }

    public Agent create(Agent agent) throws IOException, WebServicesException {
        if(StringUtils.isEmpty(agent.getNumber())) {
            throw new WebServicesException("Cannot create agent without number " + agent.toString(), 500);
        }
        verifyAgentGroupName(agent);
        HttpPost post = new HttpPost(agentsUrl);
        String returnedValue = null;
        try {
            returnedValue = executor.fillAndExecuteRequest(post, serializer.serializePost(agent));
        } catch (RequestException e) {
            throw new WebServicesException("POST - create agent : " + e.getMessage(), e.getStatus());
        }
        if (!returnedValue.isEmpty()) {
            int id = serializer.extractId(returnedValue);
            agent.setId(id);
            saveAgentMembership(agent);
        }
        return agent;
    }

    public List<Agent> list() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(agentsUrl);
        try {
            String result = executor.executeRequest(get);
            return serializer.deserializeAgents(result);
        } catch (RequestException e) {
            throw new WebServicesException("GET - list agents : " + e.getMessage(), e.getStatus());
        }
    }

    public void delete(Agent agent) throws IOException, WebServicesException {
        agent.removeAllMembership();
        saveAgentMembership(agent);
        HttpDelete delete = new HttpDelete(agentsUrl + "/" + agent.getId());
        try {
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - agent deletion : " + e.getMessage(), e.getStatus());
        }
    }

    private List<Queue> listQueues() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(queuesUrl);
        get.setHeader("X-Auth-Token", configmgtToken);
        try {
            String result = executor.executeRequest(get);
            return serializer.deserializeQueues(result);
        } catch (RequestException e) {
            throw new WebServicesException("GET - list queues : " + e.getMessage(), e.getStatus());
        }
    }

    public List<Queue> listQueuesCache() throws IOException, WebServicesException {
        ensureQueuesCache();
        return _queuesCache;
    }

    private synchronized void ensureQueuesCache() throws IOException, WebServicesException {
        if(_queuesCache == null) {
            _queuesCache = listQueues();
        }
    }

    public Queue getQueueById(Integer id) throws IOException, WebServicesException {
        ensureQueuesCache();
        for (Queue q : _queuesCache) {
            if(q.getId().equals(id)) {
                return q;
            }
        }
        return null;
    }

    public Queue getQueueByName(String name) throws IOException, WebServicesException {
        ensureQueuesCache();
        for (Queue q : _queuesCache) {
            if(q.getName().equals(name.toLowerCase())) {
                return q;
            }
        }
        return null;
    }

    public List<QueueMembership> getAgentMembership(int agentId) throws IOException, WebServicesException {
        HttpGet get = new HttpGet(agentsUrl + "/" + agentId + "/queues");
        try {
            String result = executor.executeRequest(get);
            List<QueueMembership> memberships = serializer.deserializeQueueMemberships(result);
            for(QueueMembership m: memberships) {
                m.setQueue(getQueueById(m.getQueue_id()));
            }
            return memberships;
        } catch (RequestException e) {
            throw new WebServicesException("GET - get agent membership : " + e.getMessage(), e.getStatus());
        }
    }

    private void saveAgentMembership(Agent agent) throws IOException, WebServicesException {
        if(agent.getQueuename() != null) {
            Queue q = getQueueByName(agent.getQueuename());
            if(q != null) {
                if(!agent.getMemberships().containsKey(q.getId())) {
                    agent.setMembership(q.getId(), 0);
                } else {
                    logger.warning("Agent is already member of '" + agent.getQueuename() + "', ignoring membership update for " + agent.toString());
                }
            } else {
                logger.warning("Unable to find queue '" + agent.getQueuename() + "' for " + agent.toString());
            }
        }
        addMemberships(agent.getMembershipsToAdd());
        updateMemberships(agent.getMembershipsToUpdate());
        deleteMemberships(agent.getId(), agent.getMembershipsToDelete());
    }


    private void addMemberships(Set<QueueMembership> memberships) throws IOException, WebServicesException {
        for(QueueMembership m : memberships) {
            addMembership(m);
        }
    }

    private void addMembership(QueueMembership m) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(membershipsUrl + "/" +  m.getQueue_id() + "/members/agents");

        try {
            executor.fillAndExecuteRequest(post, serializer.serializePost(m));
        } catch (RequestException e) {
            throw new WebServicesException("POST - addMembership : " + e.getMessage(), e.getStatus());
        }
    }

    private void updateMemberships(Set<QueueMembership> memberships) throws IOException, WebServicesException {
        for(QueueMembership m : memberships) {
            updateMembership(m);
        }
    }

    private void updateMembership(QueueMembership m) throws IOException, WebServicesException {
        HttpPut post = new HttpPut(membershipsUrl + "/" +  m.getQueue_id() + "/members/agents/" + m.getAgent_id());

        try {
            executor.fillAndExecuteRequest(post, serializer.serializePost(m));
        } catch (RequestException e) {
            throw new WebServicesException("POST - addMembership : " + e.getMessage(), e.getStatus());
        }
    }

    private void deleteMemberships(Integer agentId, Set<Integer> queueIds) throws IOException, WebServicesException  {
        for(Integer qid : queueIds) {
            deleteMembership(agentId, qid);
        }
    }

    private void deleteMembership(Integer agentId, Integer queueId) throws IOException, WebServicesException  {
        HttpDelete delete = new HttpDelete(membershipsUrl + "/" +  queueId + "/members/agents/" + agentId);
        try {
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - agent deletion : " + e.getMessage(), e.getStatus());
        }
    }

    public List<AgentGroup> listGroupsCache() throws IOException, WebServicesException {
        ensureGroupsCache();
        return _groupsCache;
    }

    private synchronized void ensureGroupsCache() throws IOException, WebServicesException {
        if(_groupsCache == null) {
            _groupsCache = listGroups();
        }
    }

    private List<AgentGroup> listGroups() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(agentGroupsUrl);
        get.setHeader("X-Auth-Token", configmgtToken);
        try {
            String result = executor.executeRequest(get);
            return serializer.deserializeAgentGroups(result);
        } catch (RequestException e) {
            throw new WebServicesException("GET - list groups : " + e.getMessage(), e.getStatus());
        }
    }

    public AgentGroup getGroupById(Integer id) throws IOException, WebServicesException {
        ensureGroupsCache();
        for (AgentGroup g : _groupsCache) {
            if(g.getId().equals(id)) {
                return g;
            }
        }
        return null;
    }

    public AgentGroup getGroupByName(String name) throws IOException, WebServicesException {
        ensureGroupsCache();
        for (AgentGroup g : _groupsCache) {
            if(g.getName().equals(name.toLowerCase())) {
                return g;
            }
        }
        return null;
    }

    private void verifyAgentGroupName(Agent agent) throws IOException, WebServicesException {
        if(agent.getGroupname() != null) {
            AgentGroup group = getGroupByName(agent.getGroupname());
            if(group != null) {
                agent.setNumgroup(group.getId());
            } else {
                logger.warning("Unable to find group '" + agent.getGroupname() + "' for " + agent.toString());
            }
        }
    }

}
