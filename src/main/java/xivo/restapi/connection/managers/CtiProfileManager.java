package xivo.restapi.connection.managers;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.methods.HttpGet;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.JsonSerializer.ResponseContent;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.CtiProfile;

import com.google.gson.reflect.TypeToken;

public class CtiProfileManager extends Manager {

    private String ctiProfilesUrl;
    private List<CtiProfile> cache;

    public CtiProfileManager(RequestExecutor executor, JsonSerializer serializer, String ctiProfilesUrl) {
        super(executor, serializer);
        this.ctiProfilesUrl = ctiProfilesUrl;
    }

    public List<CtiProfile> list() throws IOException, WebServicesException {
        if (cache != null)
            return cache;

        HttpGet get = new HttpGet(ctiProfilesUrl);
        try {
            String result = executor.executeRequest(get);
            cache = serializer.deserializeList(result, new TypeToken<ResponseContent<CtiProfile>>() {
            });
            return cache;
        } catch (RequestException e) {
            throw new WebServicesException("GET - list users : " + e.getMessage(), e.getStatus());
        }
    }

    public CtiProfile getCtiProfileByName(String name) throws IOException, WebServicesException {
        List<CtiProfile> profiles = list();
        for (CtiProfile profile : profiles) {
            if (profile.getName().equals(name))
                return profile;
        }
        return null;
    }

    public CtiProfile getCtiProfileById(int id) throws IOException, WebServicesException {
        List<CtiProfile> profiles = list();
        for (CtiProfile profile : profiles) {
            if (profile.getId().equals(id))
                return profile;
        }
        return null;
    }

}
