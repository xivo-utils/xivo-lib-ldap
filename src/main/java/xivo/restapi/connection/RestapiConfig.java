package xivo.restapi.connection;

public interface RestapiConfig {
    public static final String USERS_URL_FIELD = "usersUrl";
    public static final String AGENTS_URL_FIELD = "agentsUrl";
    public static final String MEMBERSHIPS_URL_FIELD = "membershipsUrl";
    public static final String VOICEMAILS_URL_FIELD = "voicemailsUrl";
    public static final String LINES_URL_FIELD = "linesUrl";
    public static final String EXTENSIONS_URL_FIELD = "extensionsUrl";
    public static final String CTI_PROFILES_URL_FIELD = "ctiProfilesUrl";
    public static final String SIP_ENDPOINTS_URL_FIELD = "sipEndpointsUrl";
    public static final String CONFIGURATION_URL_FIELD = "configurationUrl";
    public static final String DEVICES_URL_FIELD = "devicesUrl";
    public static final String RESTAPI_URL = "restapiUrl";
    public static final String EXTERNAL_CONTEXT_NAME = "externalContextName";

    public static final String CONFIGMGT_URL = "configmgtUrl";
    public static final String CONFIGMGT_TOKEN = "configmgtToken";
    public static final String CONFIGMGT_QUEUE_URL = "queuesUrl";
    public static final String CONFIGMGT_AGENT_GROUPS_URL_FIELD = "agentGroupsUrl";
    public static final String CONFIGMGT_MDS_URL = "mdsUrl";

    public String get(String name);
	public static final Integer port = 9486;
	public static final Integer configMgtPort = 9100;
}
