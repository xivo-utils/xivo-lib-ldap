package xivo.restapi.connection;

public class RequestException extends Exception {

    private static final long serialVersionUID = 1714967782428499663L;

    private int status;

    public RequestException(String message, int status) {
        super(message);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
