package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

import static java.text.MessageFormat.format;

public class ContextInterval {
    public String start;
    public String end;
    public int didLength;
    public String prefix;

    public ContextInterval(String start, String end, int didLength) {
        this.start = start;
        this.end = end;
        if(didLength == 0)
            this.didLength = start.length();
        else
            this.didLength = didLength;
        if (start.length() < this.didLength) {
            this.prefix = "";
        } else {
            this.prefix = start.substring(0, start.length() - this.didLength);
        }
    }

    public ContextInterval(String start, String end) {
        this(start, end, 0);
    }

    public boolean isNumberInbound(String number) {
        return start.compareTo(number) <= 0 && (end == null || end.compareTo(number) >= 0);
    }

    @Override
    public String toString() {
        return format("start={0}, end={1}, didLength={2}", start, end, didLength);
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }
}
