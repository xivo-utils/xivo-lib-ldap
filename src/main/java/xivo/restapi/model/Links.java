package xivo.restapi.model;

import java.util.ArrayList;

public class Links {
    public static class UserLine {
        public int line_id;
        public int user_id;

        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("line_id=");
            builder.append(line_id);
            builder.append(", ");

            builder.append("user_id=");
            builder.append(user_id);
            builder.append(", ");
            
            return builder.toString();
        }
    }

    public static class LineExtension {
        public int line_id;
        public int extension_id;

        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("line_id=");
            builder.append(line_id);
            builder.append(", ");

            builder.append("extension_id=");
            builder.append(extension_id);
            builder.append(", ");
            
            return builder.toString();
        }
    }

    public static class LineEndpoint {
        public int line_id;
        public int endpoint_id;

        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("line_id=");
            builder.append(line_id);
            builder.append(", ");

            builder.append("endpoint_id=");
            builder.append(endpoint_id);
            builder.append(", ");
            
            return builder.toString();
        }
    }

    public static class Endpoint {
        public int id;
        public ArrayList<ArrayList<String>> options;

        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("id=");
            builder.append(id);
            builder.append(", ");

            builder.append("options=");
            builder.append(options);
            builder.append(", ");
            
            return builder.toString();
        }
    }

    public static class UserVoicemail {
        public int user_id;
        public int voicemail_id;
    }

    public static class UserCti {
        public UserCti() {
        }

        public UserCti(CtiConfiguration ctiConfiguration) {
            enabled = ctiConfiguration.isEnabled();
            cti_profile_id = ctiConfiguration.getCtiProfile() == null ? null : ctiConfiguration.getCtiProfile().getId();
        }

        public Integer cti_profile_id;
        public Boolean enabled;
    }
}
