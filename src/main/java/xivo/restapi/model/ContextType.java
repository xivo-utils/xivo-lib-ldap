package xivo.restapi.model;

public enum ContextType {

    INTERNAL("internal"), OUTCALL("outcall"), INCALL("incall"), OTHERS("others");

    private String type;

    private ContextType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }

}
