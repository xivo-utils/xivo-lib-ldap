package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

import xivo.restapi.model.Line.RestExtension;

import static java.text.MessageFormat.format;

public class IncomingCall {
    private String sda;
    private String context = "from-extern";
    private Integer extensionId = null;

    public IncomingCall(String sda) {
        this.sda = sda;
    }

    public IncomingCall(String sda, String context) {
        this.sda = sda;
        this.context = context;
    }

    public IncomingCall() {
    }

    public IncomingCall(RestExtension e) {
        this.sda = e.exten;
        this.extensionId = e.id;
        this.context = e.context;
    }

    public String getSda() {
        return sda;
    }

    public void setSda(String sda) {
        this.sda = sda;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Override
    public String toString() {
        return format("sda={0}, context={1}", sda, context);
    }

    public Integer getExtensionId() {
        return extensionId;
    }

    public void setExtensionId(Integer extensionId) {
        this.extensionId = extensionId;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }
}
