package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

public class QueueMembership {
    private Integer agent_id;
    private Integer penalty;
    private Integer queue_id;
    private transient Queue queue;

    public QueueMembership() {

    }

    public QueueMembership(Integer agent_id, Integer penalty, Integer queue_id) {
        this.agent_id = agent_id;
        this.penalty = penalty;
        this.queue_id = queue_id;
    }


    public Integer getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(Integer agent_id) {
        this.agent_id = agent_id;
    }

    public Integer getPenalty() {
        return penalty;
    }

    public void setPenalty(Integer penalty) {
        this.penalty = penalty;
    }

    public Integer getQueue_id() {
        return queue_id;
    }

    public void setQueue_id(Integer queue_id) {
        this.queue_id = queue_id;
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }
}
