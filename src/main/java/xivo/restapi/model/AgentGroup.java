package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

public class AgentGroup {

    private Integer id;
    private String name;

    public AgentGroup(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AgentGroup(");
        if(id != null) {
            builder.append("id=");
            builder.append(id);
        }

        if(name != null){
            builder.append(",name=");
            builder.append(name);
        }
        builder.append(")");
        return builder.toString();
    }
    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }

}
