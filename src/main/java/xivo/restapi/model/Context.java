package xivo.restapi.model;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;

public class Context {

    private String name;
    private List<ContextInterval> intervals = new LinkedList<ContextInterval>();
    private ContextType type;
    private Integer entityID;
    
    public Context(String name) {
    	this.name = name;
    	this.type = ContextType.INTERNAL;
    	this.intervals = new LinkedList<ContextInterval>();
    	this.entityID = 1;
    }

    public Context(String name, List<ContextInterval> intervals) {
        this.name = name;
        this.intervals = intervals;
        this.type = ContextType.INTERNAL;
    }

    public Context(String name, List<ContextInterval> intervals, ContextType type) {
        this.name = name;
        this.intervals = intervals;
        this.type = type;
    }
    
    public Context(String name, List<ContextInterval> intervals, Integer entityID) {
        this.name = name;
        this.intervals = intervals;
        this.entityID = entityID;
    }

    public String getName() {
        return name;
    }

    public void setEntityID(Integer EntityID) {
        this.entityID = EntityID;
    }

    public List<ContextInterval> getIntervals() {
        return intervals;
    }

    public void addInterval(ContextInterval contextInterval) {
        intervals.add(contextInterval);

    }

    public boolean isNumberInbound(String number) {
        for (ContextInterval it : intervals)
            if (it.isNumberInbound(number))
                return true;
        return false;
    }

    public ContextType getType() {
        return type;
    }
    
    public Integer getEntityID() {
    	return entityID;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    public ContextInterval getIntervalForNumber(String number) {
        for (ContextInterval it : intervals)
            if (it.isNumberInbound(number))
                return it;
        return null;
    }
}
