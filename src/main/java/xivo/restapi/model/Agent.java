package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Agent {

    private Integer id;
    private String firstname;
    private String lastname;
    private String number;
    private Integer numgroup;
    private String context;
    private String language;
    private String description;
    private String passwd;
    private transient Map<Integer, QueueMembership> memberships = new HashMap<>();
    private transient Set<Integer> membershipsToAdd = new HashSet<>();
    private transient Set<Integer> membershipsToUpdate = new HashSet<>();
    private transient Set<Integer> membershipsToDelete = new HashSet<>();
    private transient String groupname;
    private transient String queuename;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getNumgroup() {
        return numgroup;
    }

    public void setNumgroup(Integer numgroup) {
        this.numgroup = numgroup;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Map<Integer, QueueMembership> getMemberships() {
        return memberships;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getQueuename() {
        return queuename;
    }

    public void setQueuename(String queuename) {
        this.queuename = queuename;
    }

    public void initMemberships(List<QueueMembership> memberships) {
        for(QueueMembership m: memberships) {
            this.memberships.put(m.getQueue_id(), m);
        }
    }

    public void setMembership(Integer queue_id, Integer penalty) {
        if(this.memberships.containsKey(queue_id)) {
            this.membershipsToUpdate.add(queue_id);
        } else {
            this.membershipsToAdd.add(queue_id);
        }
        this.memberships.put(queue_id, new QueueMembership(id, penalty, queue_id));
    }

    private QueueMembership setMembershipAgentId(QueueMembership m) {
        m.setAgent_id(id);
        return m;
    }

    public Set<QueueMembership> getMembershipsToAdd() {
        return membershipsToAdd.stream()
                .map(qid -> this.memberships.get(qid))
                .map(m -> setMembershipAgentId(m))
                .collect(Collectors.toSet());
    }

    public Set<QueueMembership> getMembershipsToUpdate() {
        return membershipsToUpdate.stream()
                .map(qid -> this.memberships.get(qid))
                .map(m -> setMembershipAgentId(m))
                .collect(Collectors.toSet());
    }

    public Set<Integer> getMembershipsToDelete() {
        return membershipsToDelete;
    }

    public void removeMembership(Integer queue_id) {
        this.memberships.remove(queue_id);
        this.membershipsToDelete.add(queue_id);
    }

    public void removeAllMembership() {
        Set<Integer> toRm = this.memberships
                .values()
                .stream()
                .map(m -> m.getQueue_id())
                .collect(Collectors.toSet());

        toRm.stream().forEach(i -> removeMembership(i));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (id != null) {
            builder.append("id=");
            builder.append(id);
            builder.append(", ");
        }
        if (firstname != null) {
            builder.append("firstname=");
            builder.append(firstname);
        }
        if (lastname != null) {
            builder.append("lastname=");
            builder.append(lastname);
        }
        if (number != null) {
            builder.append("number=");
            builder.append(number);
        }
        if (numgroup != null) {
            builder.append("numgroup=");
            builder.append(numgroup);
        }
        if (context != null) {
            builder.append("context=");
            builder.append(context);
        }
        if (language != null) {
            builder.append("language=");
            builder.append(language);
        }
        if (description != null) {
            builder.append("description=");
            builder.append(description);
        }
        if (passwd != null) {
            builder.append("passwd=");
            builder.append(passwd);
        }


        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }

}
