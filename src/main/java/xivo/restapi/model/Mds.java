package xivo.restapi.model;

public class Mds {
    private Long id;
    private String name;
    private String display_name;
    private String voip_ip;

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return display_name;
    }

    public void setDisplayName(String displayName) { this.display_name = displayName; }

    public String getVoipIp() {
        return voip_ip;
    }

    public void setVoipId(String voipId) {
        this.voip_ip = voipId;
    }
}
