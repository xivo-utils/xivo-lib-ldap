package xivo.ldap.xivoconnection;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import xivo.restapi.model.Context;
import xivo.restapi.model.ContextInterval;

import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestNumberGeneratorFactory {
    private PostgresConnector postgres;
    private NumberGeneratorFactory factory;

    @Before
    public void setUp() {
        postgres = EasyMock.createMock(PostgresConnector.class);
        factory = new NumberGeneratorFactory(postgres);
    }

    @Test
    public void testCreateLineNumberGenerator() throws EmptyContextException, SQLException {
        String contextName = "default";
        List<String> nums = Collections.singletonList("1000");
        Context context = new Context(contextName, Collections.singletonList(new ContextInterval("1000", "1009")));
        EasyMock.expect(postgres.getContext(contextName, "user")).andReturn(context);
        EasyMock.expect(postgres.getExtensionsInContext(contextName)).andReturn(nums);
        EasyMock.replay(postgres);

        factory = new NumberGeneratorFactory(postgres);
        NumberGenerator gen = factory.createLineNumberGenerator(contextName);

        assertEquals(gen.context, context);
        assertEquals(gen.existingNumbers, nums);
    }

    @Test
    public void testCreateSdaNumberGenerator() throws EmptyContextException, SQLException {
        String contextName = "from-extern";
        List<String> nums = Collections.singletonList("0230210092");
        Context context = new Context(contextName, Collections.singletonList(new ContextInterval("0230210090", "0230210099")));
        EasyMock.expect(postgres.getContext(contextName, "incall")).andReturn(context);
        EasyMock.expect(postgres.getExtensionsInContext(contextName)).andReturn(nums);
        EasyMock.replay(postgres);

        factory = new NumberGeneratorFactory(postgres);
        NumberGenerator gen = factory.createSdaGenerator(contextName);

        assertEquals(gen.context, context);
        assertEquals(gen.existingNumbers, nums);
    }
}
