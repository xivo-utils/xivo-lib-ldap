package xivo.ldap.xivoconnection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.*;

public class TestMockXivoConnector {

    private MockXivoConnector mockXivoConnector;
    private RealXivoConnector realXivoConnector;
    private User user1, user2;
    private Voicemail voicemail1, voicemail2;
    private IncomingCall incomingCall;
    private String reportFolder;
    private File logFile;

    @Before
    public void setUp() throws Exception {
        this.realXivoConnector = EasyMock.createMock(RealXivoConnector.class);
        this.reportFolder = "reports/";
        this.mockXivoConnector = new MockXivoConnector(realXivoConnector, reportFolder);
        user1 = new User();
        user2 = new User();
        user1.setFirstname("John");
        user1.setDescription("my first description");
        user2.setFirstname("Jack");
        user2.setDescription("my second description");
        voicemail1 = new Voicemail();
        voicemail1.setNumber("123");
        voicemail2 = new Voicemail();
        voicemail2.setNumber("456");
        incomingCall = new IncomingCall("5000", "from_extern");

        logFile = new File(reportFolder + MockXivoConnector.MOCK_XIVO_UPDATE_FILE);
    }

    @Test
    public void testCreateUsers() throws IOException, WebServicesException {
        mockXivoConnector.createUser(user1);
        testFileContent("User creation: ", Arrays.asList(new User[] { user1 }));
    }

    @Test
    public void testGetAllUsers() throws IOException, WebServicesException, SQLException {
        List<User> expectedResult = new LinkedList<User>();
        EasyMock.expect(realXivoConnector.getAllUsers()).andReturn(expectedResult);
        EasyMock.replay(realXivoConnector);

        List<User> result = mockXivoConnector.getAllUsers();
        assertSame(result, expectedResult);
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testUpdateUsers() throws IOException {
        mockXivoConnector.updateUser(user1);
        testFileContent("User update: ", Arrays.asList(new User[] { user1 }));
    }

    @Test
    public void testGetIncomingCallForUser() throws IOException {
        EasyMock.expect(realXivoConnector.getIncomingCallForUser(user1)).andReturn(incomingCall);
        EasyMock.replay(realXivoConnector);

        IncomingCall result = mockXivoConnector.getIncomingCallForUser(user1);
        assertSame(incomingCall, result);
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testGetFreeSdas() throws IOException {
        List<String> expectedResult = new LinkedList<String>();
        EasyMock.expect(realXivoConnector.getFreeSdas()).andReturn(expectedResult);
        EasyMock.replay(realXivoConnector);

        List<String> result = mockXivoConnector.getFreeSdas();
        assertSame(expectedResult, result);
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testGetXivoInfo() throws IOException, WebServicesException {
        Info info = EasyMock.createMock(Info.class);
        EasyMock.expect(realXivoConnector.getXivoInfo()).andReturn(info);
        EasyMock.replay(realXivoConnector);

        Info result = mockXivoConnector.getXivoInfo();
        assertSame(info, result);
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testAssociateIncallsToUsers() throws IOException {
        IncomingCall incall1 = new IncomingCall("1234");
        user1.setIncomingCall(incall1);

        mockXivoConnector.associateIncallToUser(user1);

        String strUser1 = user1.getFullname() + " has SDA " + user1.getIncomingCall().getSda();
        List<String> expectedStrings = Arrays.asList(new String[] { strUser1 });
        testFileContent("", expectedStrings);
    }

    private void testFileContent(String prefix, List<?> objects) throws IOException {
        assertTrue(logFile.exists());
        BufferedReader reader = new BufferedReader(new FileReader(logFile));
        for (Object object : objects) {
            assertEquals(prefix + object.toString(), reader.readLine());
        }
        reader.close();
    }

    @Test
    public void testCreateLinesForUsers() throws IOException {
        user1.setLine(new Line("1234"));
        User user3 = new User();

        mockXivoConnector.createLineForUser(user1);
        mockXivoConnector.createLineForUser(user3);

        String str1 = "Line creation: " + user1.getLine();
        String str2 = "User - line association : user " + user1.getFirstname() + " " + user1.getLastname()
                + " has the line " + user1.getLine().getNumber();
        List<String> strings = Arrays.asList(new String[] { str1, str2 });

        testFileContent("", strings);
    }

    @Test
    public void testCreateVoicemailsForUsers() throws IOException {
        user1.setVoicemail(voicemail1);
        mockXivoConnector.createVoicemailForUser(user1);

        String expectedString = user1.getFullname() + " has the voicemail " + user1.getVoicemail();
        testFileContent("Voicemail creation: ", Arrays.asList(new String[] { expectedString }));
    }

    @Test
    public void testUpdateLinesForUsers() throws IOException {
        user1.setLine(new Line("1234"));
        mockXivoConnector.updateLineForUser(user1);

        String expectedString = user1.getFullname() + " has the line " + user1.getLine().getNumber();
        testFileContent("Line update: ", Arrays.asList(new String[] { expectedString }));
    }

    @Test
    public void testUpdateVoicemailsForUsers() throws IOException {
        user1.setVoicemail(voicemail1);
        mockXivoConnector.updateVoicemailForUser(user1);

        String expectedString = user1.getFullname() + " has the voicemail " + user1.getVoicemail();
        testFileContent("Voicemail update: ", Arrays.asList(new String[] { expectedString }));
    }

    @Test
    public void testDeleteUsers() throws IOException {
        mockXivoConnector.deleteUser(user1);
        testFileContent("User deletion: ", Arrays.asList(new User[] { user1 }));
    }

    @Test
    public void testDeleteLinesForUsers() throws IOException {
        user1.setLine(new Line("123"));
        User user3 = new User();
        user3.setFirstname("Albert");

        mockXivoConnector.deleteLineForUser(user1);
        mockXivoConnector.deleteLineForUser(user3);

        List<String> lines = new LinkedList<String>();
        lines.add("line number " + user1.getLine().getNumber() + " for user " + user1.getFullname());
        testFileContent("Line deletion: ", lines);
    }

    @Test
    public void testDeleteVoicemailsForUsers() throws IOException {
        List<String> lines = new LinkedList<String>();
        user1.setVoicemail(voicemail1);
        lines.add("voicemail " + user1.getVoicemail() + " for user " + user1.getFullname());

        mockXivoConnector.deleteVoicemailForUser(user1);
        testFileContent("Voicemail deletion: ", lines);
    }

    @Test
    public void testDeleteIncallsForUsers() throws IOException {
        List<String> lines = new LinkedList<String>();
        user1.setIncomingCall(new IncomingCall("1234"));
        lines.add("incoming call " + user1.getIncomingCall().getSda() + " for user " + user1.getFullname());

        mockXivoConnector.deleteIncallForUser(user1);
        testFileContent("Incoming call deletion: ", lines);
    }

    @Test
    public void testUpdateIncallsForUsers() throws IOException {
        List<String> lines = new LinkedList<String>();
        user1.setIncomingCall(new IncomingCall("1234"));
        String str = "incoming call " + user1.getIncomingCall().getSda() + " for user " + user1.getFullname();
        lines.add(str);

        mockXivoConnector.updateIncallForUser(user1);
        testFileContent("Incoming call update: ", lines);
    }

    @Test
    public void testCreateIncallsForUsers() throws IOException {
        List<String> lines = new LinkedList<String>();
        user1.setIncomingCall(new IncomingCall("1234"));
        String str = "incoming call " + user1.getIncomingCall().getSda() + " for user " + user1.getFullname();
        lines.add(str);
        mockXivoConnector.createIncallForUser(user1);
        testFileContent("Incoming call creation: ", lines);
    }

    @Test
    public void testGetDefaultCtiProfile() throws IOException, WebServicesException {
        CtiProfile profile = new CtiProfile();
        EasyMock.expect(realXivoConnector.getDefaultCtiProfile()).andReturn(profile);
        EasyMock.replay(realXivoConnector);

        CtiProfile result = mockXivoConnector.getDefaultCtiProfile();

        assertSame(result, profile);
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testGetUser() throws IOException, WebServicesException, SQLException {
        int userId = 5;
        User expectedUser = new User();
        EasyMock.expect(realXivoConnector.getUser(userId)).andReturn(expectedUser);
        EasyMock.replay(realXivoConnector);

        User result = mockXivoConnector.getUser(userId);
        assertSame(result, expectedUser);
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testgetUsersNumber() throws IOException, WebServicesException, SQLException {
        int number = 54;
        EasyMock.expect(realXivoConnector.getUsersNumber()).andReturn(number);
        EasyMock.replay(realXivoConnector);

        int result = mockXivoConnector.getUsersNumber();
        assertSame(number, result);
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testCreateUser() throws IOException, WebServicesException {
        mockXivoConnector.createUser(user1);

        testFileContent("User creation: ", Arrays.asList(new User[] { user1 }));
    }

    @Test
    public void testExtensionsExist() throws SQLException {
        String exten = "2000";
        String context = "test";
        EasyMock.expect(realXivoConnector.extensionExists(exten, context)).andReturn(true);
        EasyMock.replay(realXivoConnector);

        assertTrue(mockXivoConnector.extensionExists(exten, context));
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testGetExtensionsForContext() throws SQLException {
        List<String> result = Arrays.asList(new String[] { "2000", "2001", "2005" });
        String context = "test";
        EasyMock.expect(realXivoConnector.getExtensionsInContext(context)).andReturn(result);
        EasyMock.replay(realXivoConnector);

        assertEquals(result, mockXivoConnector.getExtensionsInContext(context));
        EasyMock.verify(realXivoConnector);
    }

    @Test
    public void testListMds() throws IOException, WebServicesException {
        List<Mds> expectedResult = new LinkedList<>();
        EasyMock.expect(realXivoConnector.listMds()).andReturn(expectedResult);
        EasyMock.replay(realXivoConnector);

        List<Mds> result = mockXivoConnector.listMds();
        assertSame(expectedResult, result);
        EasyMock.verify(realXivoConnector);
    }
}
