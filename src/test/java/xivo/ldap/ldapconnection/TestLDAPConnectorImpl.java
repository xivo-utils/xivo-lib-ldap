package xivo.ldap.ldapconnection;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.naming.CommunicationException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.User;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ TestLDAPConnectorImpl.class, BasicAttribute.class, ModificationItem.class, LDAPConnectorImpl.class })
public class TestLDAPConnectorImpl extends TestCase {

    private class MockUserModifier implements UserModifier {

        private boolean called = false;

        @Override
        public User modifyXivoUser(User user, SearchResult searchResult) throws NamingException {
            assertFalse("Should not happen", true);
            return user;
        }

        @Override
        public User modifyLdapUser(User user) {
            called = true;
            return user;
        }

    }

    private LDAPConnectorImpl ldapConnector;
    private ContextFactory mockFactory;
    private LdapContext mockContext;
    private String ldapRoot;
    private LDAPDeserializer ldapDeserializer;
    private String searchCriteria = "(objectClass=myClass)";

    @Before
    public void setUp() throws Exception {
        mockFactory = PowerMock.createNiceMock(ContextFactory.class);
        mockContext = PowerMock.createNiceMock(LdapContext.class);
        ldapRoot = "dc=root";
        ldapDeserializer = PowerMock.createNiceMock(LDAPDeserializer.class);
        EasyMock.expect(mockFactory.get()).andReturn(mockContext);
        EasyMock.replay(mockFactory);
        MockUserModifier userModifier = new MockUserModifier();
        ldapConnector = new LDAPConnectorImpl(mockFactory, ldapRoot, ldapDeserializer, null, searchCriteria, userModifier);
        EasyMock.reset(mockFactory);
    }

    @Test
    public void testUpdateUsers() throws Exception {
        User user = new User();
        user.setLine(new Line("123"));
        user.setIncomingCall(new IncomingCall("0123456789"));
        UpdateRequest request = new UpdateRequest("(uid=1234)");
        request.addLdapField("field1", "123");

        LDAPConnectorImpl ldapConnector = PowerMock.createNicePartialMockForAllMethodsExcept(LDAPConnectorImpl.class,
                "updateUsers", "setContext", "setUpdateRequestFactory", "setUserModifier");
        ldapConnector.setContext(mockContext);
        MockUserModifier userModifier = new MockUserModifier();
        ldapConnector.setUserModifier(userModifier);
        UpdateRequestFactory factory = PowerMock.createNiceMock(UpdateRequestFactory.class);
        ldapConnector.setUpdateRequestFactory(factory);
        Whitebox.setInternalState(ldapConnector, "logger", Logger.getLogger("JUnit"));

        Set<String> updatedFields = new HashSet<String>();
        updatedFields.add("lineNumber");
        updatedFields.add("telephoneNumber");
        updatedFields.add("mobile");
        EasyMock.expect(factory.createUpdateRequest(user, updatedFields)).andReturn(request);
        PowerMock.expectPrivate(ldapConnector, "getDN", "(uid=1234)").andReturn("cn=value1");
        BasicAttribute attribute1 = PowerMock.createNiceMock(BasicAttribute.class);
        PowerMock.expectNew(BasicAttribute.class, "field1", "123").andReturn(attribute1);
        ModificationItem item1 = PowerMock.createNiceMock(ModificationItem.class);
        PowerMock.expectNew(ModificationItem.class, DirContext.REPLACE_ATTRIBUTE, attribute1).andReturn(item1);
        mockContext.modifyAttributes(EasyMock.eq("cn=value1"), EasyMock.aryEq(new ModificationItem[] { item1}));
        PowerMock.replay(mockContext, ModificationItem.class, BasicAttribute.class, ldapConnector, factory);

        ldapConnector.updateUsers(Arrays.asList(new User[] { user }), updatedFields);
        PowerMock.verify(mockContext, ModificationItem.class, BasicAttribute.class, ldapConnector, factory);
        assertTrue(userModifier.called);
    }

    @Test
    public void testGetDN() throws Exception {
        @SuppressWarnings("unchecked")
        NamingEnumeration<SearchResult> resultMock = PowerMock.createNiceMock(NamingEnumeration.class);
        EasyMock.expect(
                mockContext.search(EasyMock.eq(this.ldapRoot), EasyMock.eq("(uid=123)"),
                        EasyMock.notNull(SearchControls.class))).andReturn(resultMock);
        EasyMock.expect(resultMock.hasMoreElements()).andReturn(true);
        SearchResult searchResult = PowerMock.createNiceMock(SearchResult.class);
        EasyMock.expect(resultMock.nextElement()).andReturn(searchResult);
        EasyMock.expect(searchResult.getNameInNamespace()).andReturn("cn=value");
        PowerMock.replay(mockContext, resultMock, searchResult);

        String result = Whitebox.invokeMethod(ldapConnector, "getDN", "(uid=123)");
        assertEquals("cn=value", result);
        PowerMock.verify(mockContext, resultMock, searchResult);
    }

    @Test
    public void testReconnectAndGetDN() throws Exception {
        @SuppressWarnings("unchecked")
        NamingEnumeration<SearchResult> resultMock = PowerMock.createNiceMock(NamingEnumeration.class);
        EasyMock.expect(
                mockContext.search(EasyMock.eq(this.ldapRoot), EasyMock.eq("(uid=123)"),
                        EasyMock.notNull(SearchControls.class))).andThrow(new CommunicationException());
        EasyMock.expect(mockFactory.get()).andReturn(mockContext);
        EasyMock.expect(
                mockContext.search(EasyMock.eq(this.ldapRoot), EasyMock.eq("(uid=123)"),
                        EasyMock.notNull(SearchControls.class))).andReturn(resultMock);
        EasyMock.expect(resultMock.hasMoreElements()).andReturn(true);
        SearchResult searchResult = PowerMock.createNiceMock(SearchResult.class);
        EasyMock.expect(resultMock.nextElement()).andReturn(searchResult);
        EasyMock.expect(searchResult.getNameInNamespace()).andReturn("cn=value");
        PowerMock.replayAll();

        String result = Whitebox.invokeMethod(ldapConnector, "getDN", "(uid=123)");
        assertEquals("cn=value", result);
        PowerMock.verifyAll();
    }

    @Test
    public void testGetUsers() throws NamingException {
        @SuppressWarnings("unchecked")
        NamingEnumeration<SearchResult> resultMock = PowerMock.createNiceMock(NamingEnumeration.class);
        List<User> expectedResult = new LinkedList<User>();
        EasyMock.expect(
                mockContext.search(EasyMock.eq(this.ldapRoot), EasyMock.eq(searchCriteria),
                        EasyMock.notNull(SearchControls.class))).andReturn(resultMock);
        EasyMock.expect(ldapDeserializer.createAllUsers(resultMock)).andReturn(expectedResult);
        PowerMock.replay(mockContext, ldapDeserializer);

        List<User> result = ldapConnector.getUsers();
        PowerMock.verify(mockContext, ldapDeserializer);
        assertEquals(expectedResult, result);
    }
}
