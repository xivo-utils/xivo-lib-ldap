package xivo.ldap.ldapconnection;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class TestFieldMapping extends TestCase {

    @Test
    public void testGetXivoField() {
        FieldMapping fieldMapping = new FieldMapping();
        fieldMapping.addFieldMapping("sn", "User.lastname");
        assertTrue(fieldMapping.getXivoFields("sn").contains("User.lastname"));
    }

    @Test
    public void testAddFieldMapping() {
        FieldMapping fieldMapping = new FieldMapping();
        fieldMapping.addFieldMapping("sn", "User.lastname");
        fieldMapping.addFieldMapping("sn", "Voicemail.fullname");
        Map<String, String> map = Whitebox.getInternalState(fieldMapping, "map");
        Set<String> expectedSet = new HashSet<String>();
        expectedSet.add("User.lastname");
        expectedSet.add("Voicemail.fullname");
        assertEquals(expectedSet, map.get("sn"));

    }

    @Test
    public void testGetLdapFields() {
        FieldMapping fieldMapping = new FieldMapping();
        fieldMapping.addFieldMapping("sn", "lastname");
        fieldMapping.addFieldMapping("givenName", "firstname");
        Set<String> set = new HashSet<String>();
        set.add("sn");
        set.add("givenName");
        assertEquals(set, fieldMapping.getLdapFields());
    }

    @Test
    public void testGetLdapField() {
        FieldMapping fieldMapping = new FieldMapping();
        fieldMapping.addFieldMapping("telephoneNumber", "Line.number");
        fieldMapping.addFieldMapping("telephoneNumber", "Voicemail.mailbox");
        fieldMapping.addFieldMapping("givenName", "User.firstname");

        assertEquals("telephoneNumber", fieldMapping.getLdapField("Line.number"));
        assertEquals("telephoneNumber", fieldMapping.getLdapField("Voicemail.mailbox"));
        assertEquals("givenName", fieldMapping.getLdapField("User.firstname"));
    }
}
