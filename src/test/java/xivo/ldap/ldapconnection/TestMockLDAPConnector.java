package xivo.ldap.ldapconnection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.directory.SearchResult;

import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;

import xivo.restapi.model.User;

public class TestMockLDAPConnector extends TestCase {

    private class MockUserModifier implements UserModifier {

        private boolean called = false;

        @Override
        public User modifyXivoUser(User user, SearchResult searchResult) throws NamingException {
            assertFalse("Should not happen", true);
            return user;
        }

        @Override
        public User modifyLdapUser(User user) {
            called = true;
            return user;
        }

    }

    private MockLDAPConnector connector;
    private LDAPConnectorImpl realConnector;
    private MockUserModifier userModifier;
    private String reportFolder;
    private UpdateRequestFactory updateRequestFactory;

    @Before
    public void setUp() throws Exception {
        realConnector = PowerMock.createNiceMock(LDAPConnectorImpl.class);
        reportFolder = "reports/";
        updateRequestFactory = PowerMock.createNiceMock(UpdateRequestFactory.class);
        userModifier = new MockUserModifier();
        connector = new MockLDAPConnector(realConnector, userModifier, reportFolder, updateRequestFactory);
    }

    @After
    public void tearDown() {
        PowerMock.resetAll();
    }

    @Test
    public void testGetUsers() throws NamingException {
        List<User> result = new LinkedList<User>();
        EasyMock.expect(realConnector.getUsers()).andReturn(result);
        EasyMock.replay(realConnector);

        List<User> actualResult = connector.getUsers();
        assertSame(result, actualResult);
        EasyMock.verify(realConnector);
    }

    @Test
    public void testUpdateUsers() throws IOException, NoSuchFieldException, IllegalAccessException {
        User user1 = new User(), user2 = new User();
        List<User> users = Arrays.asList(new User[] { user1, user2 });
        UpdateRequest request1 = new UpdateRequest("(criteria1)");
        request1.addLdapField("lineNumber", "123");
        request1.addLdapField("sdaPhoneNumber", "0123456789");
        UpdateRequest request2 = new UpdateRequest("(criteria2)");
        request2.addLdapField("lineNumber", "456");
        request2.addLdapField("sdaPhoneNumber", "0102456789");

        Set<String> updatedFields = new HashSet<String>();
        updatedFields.add("homePhone");
        updatedFields.add("telephoneNumber");
        EasyMock.expect(updateRequestFactory.createUpdateRequest(user1, updatedFields)).andReturn(request1);
        EasyMock.expect(updateRequestFactory.createUpdateRequest(user2, updatedFields)).andReturn(request2);
        EasyMock.replay(updateRequestFactory);

        connector.updateUsers(users, updatedFields);

        checkFileContent(Arrays.asList("\\(criteria1\\) :.*sdaPhoneNumber=0123456789.*",
                "\\(criteria2\\) :.*sdaPhoneNumber=0102456789.*"));
        checkFileContent(Arrays.asList("\\(criteria1\\) :.*lineNumber=123.*",
                "\\(criteria2\\) :.*lineNumber=456.*"));
        EasyMock.verify(updateRequestFactory);
        assertTrue(userModifier.called);
    }

    private void checkFileContent(List<String> expectedLines) throws IOException {
        File log = new File(reportFolder + MockLDAPConnector.MOCK_LDAP_UPDATE_FILE);
        assertTrue(log.exists());
        List<String> lines = FileUtils.readLines(log);
        assertEquals(expectedLines.size(), lines.size());
        int i = 0;
        for (String line : lines) {
            String expectedLine = expectedLines.get(i);
            assertTrue(line + " did not match " + expectedLine, line.matches(expectedLine));
            i++;
        }
    }

}
