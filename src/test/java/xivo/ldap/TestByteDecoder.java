package xivo.ldap;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestByteDecoder {

    @Test
    public void testGetADHexString() {
        byte[] bytes = new byte[2];
        bytes[0] = new Integer(0xa5).byteValue();
        bytes[1] = new Integer(0xc).byteValue();

        assertEquals("\\a5\\0c", new ByteDecoder().getADHexString(bytes));
    }

}
