package xivo.ldap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.powermock.reflect.Whitebox;

import xivo.restapi.model.*;

public class TestUserIntrospector {

    @Test(expected = ClassNotFoundException.class)
    public void testGetObjectFromUserException() throws Exception {
        User user = new User();
        String objectClass = "unknown";
        Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
    }

    @Test
    public void testGetObjectFromUser() throws Exception {
        User user = new User();
        String objectClass = "User";
        Object result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(User.class, result.getClass());
        assertEquals(user, result);

        objectClass = "Voicemail";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(null, result);

        Line line = new Line("1234");
        user.setLine(line);
        objectClass = "Line";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(Line.class, result.getClass());
        assertEquals(line, result);

        objectClass = "IncomingCall";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(null, result);
    }

    @Test
    public void testGetAgentFromUser() throws Exception {
        User user = new User();
        String objectClass = "User";

        objectClass = "Agent";
        Object result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(null, result);

        Agent agent = new Agent();
        user.setAgent(agent);

        result = Whitebox.invokeMethod(UserIntrospector.class, "getObjectFromUser", user, objectClass);
        assertEquals(Agent.class, result.getClass());
        assertEquals(agent, result);
    }

    @Test
    public void testGetOrCreateObjectWithUser() throws Exception {
        User user = new User();
        String objectClass = "User";
        Object result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(User.class, result.getClass());
        assertEquals(user, result);

        objectClass = "Voicemail";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(Voicemail.class, result.getClass());
        assertNotNull(user.getVoicemail());

        objectClass = "Line";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(Line.class, result.getClass());
        assertNotNull(user.getLine());

        objectClass = "IncomingCall";
        result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(IncomingCall.class, result.getClass());
        assertNotNull(user.getIncomingCall());
    }

    @Test
    public void testGetOrCreateAgentWithUser() throws Exception {
        User user = new User();

        String objectClass = "Agent";
        Object result = Whitebox.invokeMethod(UserIntrospector.class, "getOrCreateObjectWithUser", user, objectClass);
        assertEquals(Agent.class, result.getClass());
        assertNotNull(user.getAgent());
    }

    @Test
    public void testGetFirstLevelUserField()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        User user = new User();
        String mobile = "0621005497";
        user.setMobilephonenumber(mobile);

        Object result = UserIntrospector.getUserField(user, "User.mobile_phone_number");

        assertEquals(mobile, result);

        String email = "gesnaud@avencall.com";
        user.setEmail(email);

        result = UserIntrospector.getUserField(user, "User.email");

        assertEquals(email, result);
    }

    @Test
    public void testGetSecondLevelUserField()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        User user = new User();
        String number = "1000";
        user.setLine(new Line(number));

        Object result = UserIntrospector.getUserField(user, "Line.number");

        assertEquals(number, result);

        Voicemail voicemail = new Voicemail();
        voicemail.setId(3);
        voicemail.setNumber(number);
        voicemail.setName("Gregory ESNAUD");
        voicemail.setEmail("gesnaud@avencall.com");
        voicemail.setLanguage("fr_FR");
        user.setVoicemail(voicemail);

        result = UserIntrospector.getUserField(user, "Voicemail.language");
        assertEquals("fr_FR", result);

        voicemail.setLanguage("es_ES");
        result = UserIntrospector.getUserField(user, "Voicemail.language");
        assertEquals("es_ES", result);

    }

    @Test
    public void testSetFirstLevelUserField()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        User user = new User();
        String mobile = "0689417523";

        UserIntrospector.setUserField(user, "User.mobile_phone_number", mobile);

        assertEquals(mobile, user.getMobilephonenumber());

        String email = "gesnaud@avencall.com";

        UserIntrospector.setUserField(user, "User.email", email);

        assertEquals(email, user.getEmail());
    }

    @Test
    public void testSetSecondLevelUserField()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        User user = new User();
        String number = "1000";

        UserIntrospector.setUserField(user, "Line.number", number);

        assertEquals(number, user.getLine().getNumber());
    }

    /**
     * attributes are retrieved as String from the LDAP but may be represented
     * as Integer in the model
     * 
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    @Test
    public void testSetNonStringUserField()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        User user = new User();
        String simult_calls = "4";

        UserIntrospector.setUserField(user, "User.simult_calls", simult_calls);

        assertEquals(user.getSimultCalls().intValue(), 4);
    }

    @Test
    public void testGetSuperclassUserField()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        class LocalUser extends User {
        }

        LocalUser user = new LocalUser();
        user.setId(14);

        assertEquals(14, UserIntrospector.getUserField(user, "User.id"));

        class VeryLocalUser extends LocalUser {
        }

        VeryLocalUser user2 = new VeryLocalUser();
        user2.setId(87);

        assertEquals(87, UserIntrospector.getUserField(user2, "User.id"));
    }

    @Test(expected = NoSuchFieldException.class)
    public void testGetUnexistingUserfieldFails()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        User user = new User();
        UserIntrospector.getUserField(user, "User.trucmuch");
    }

    @Test
    public void testSetSuperclassUserfield()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        class LocalUser extends User {
        }
        LocalUser user = new LocalUser();

        UserIntrospector.setUserField(user, "User.id", "14");

        assertEquals(14, user.getId().intValue());

        class VeryLocalUser extends LocalUser {
        }
        VeryLocalUser user2 = new VeryLocalUser();

        UserIntrospector.setUserField(user2, "User.id", "18");

        assertEquals(18, user2.getId().intValue());
    }

    @Test(expected = NoSuchFieldException.class)
    public void testSetUnexistingUserfieldFails()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        User user = new User();
        UserIntrospector.setUserField(user, "User.trucmuch", "foobar");
    }

    @Test
    public void testSetAgentGroupName() throws Exception {
        User user = new User();
        String objectClass = "User";

        UserIntrospector.setUserField(user, "Agent.groupname", "Sales");

        assertEquals(user.getAgent().getGroupname(), "Sales");
    }


}
