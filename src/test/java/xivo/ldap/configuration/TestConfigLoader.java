package xivo.ldap.configuration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import xivo.ldap.ldapconnection.FieldMapping;

public class TestConfigLoader extends TestCase {

    private String connectorFileName;
    private String restapiFileName;
    private File connectorFile;
    private File restapiFile;
    private String generalString;

    @Before
    public void setUp() throws Exception {
        connectorFileName = "config/config.ini";
        restapiFileName = "config/general.ini";
        generalString = ";NE PAS MODIFIER CE FICHIER\n\n" + "[General]\n" + "reportFolder=/somewhere\n" + "[Restapi]\n"
                + "voicemailsUrl=myVoicemailsUrl\n" + "usersUrl=myUsersUrl\n" + "linesUrl=myLinesUrl\n"
                + "extensionsUrl=myExtenionsUrl\n" + "userLinksUrl=myUserLinksUrl\n";
        writeGeneralFile(generalString);
        Field instance = ConfigLoader.class.getDeclaredField("configuration");
        instance.setAccessible(true);
        instance.set(null, null);
    }

    @After
    public void tearDown() {
        connectorFile.delete();
        restapiFile.delete();
    }

    @Test
    public void testLoadConfiguration() throws IOException {
        String xivoSection =
                "[XiVO]\n"
                + "XiVOIP=localhost\n"
                + "restapiPort=50050\n"
                + "context=mycontext\n"
                + "reenableLiveReload=true\n"
                + "installationId=XiVOMain\n"
                ;

        String connectorString = "[FieldMapping]\n" + "User.firstname=sn\n" + "Line.number=telephoneNumber\n"
                + "User.lastname=givenName\n" + "User.password=givenName\n"
                + xivoSection
                + "[Actions]\n" + "User=C,U,D\n" + "Voicemail=U\n" + "Agent=C,U\n"
                + "Line=D\n" + "updatedLdapFields=Line.number,User.mobile\n" + "[LDAP]\n"
                + "ldapSynchronizationFilter=(objectClass=*)\n" + "ldapUrl=myLdapUrl\n" + "ldapLogin=ldapLogin\n"
                + "ldapPassword=ldapPassword\n" + "ldapRoot=ldapRoot\n" + "useTls=true\n";
        writeConnnectorFile(connectorString);

        Configuration config = ConfigLoader.getConfig();

        assertEquals("ldapLogin", config.get("ldapLogin"));
        assertEquals("ldapPassword", config.get("ldapPassword"));
        assertEquals("ldapRoot", config.get("ldapRoot"));
        assertEquals("myVoicemailsUrl", config.get("voicemailsUrl"));
        assertEquals("myUsersUrl", config.get("usersUrl"));
        assertEquals("myLinesUrl", config.get("linesUrl"));
        assertEquals("myExtenionsUrl", config.get("extensionsUrl"));
        assertEquals("myUserLinksUrl", config.get("userLinksUrl"));
        assertEquals("myLdapUrl", config.get("ldapUrl"));
        assertEquals("mycontext", config.get("context"));
        FieldMapping fieldMapping = config.getFieldMapping();
        Set<String> expectedSet = new HashSet<String>();
        expectedSet.add("User.firstname");
        assertEquals(fieldMapping.getXivoFields("sn"), expectedSet);
        expectedSet.clear();
        expectedSet.add("Line.number");
        assertEquals(fieldMapping.getXivoFields("telephoneNumber"), expectedSet);
        expectedSet.clear();
        expectedSet.add("User.lastname");
        expectedSet.add("User.password");
        assertEquals(fieldMapping.getXivoFields("givenName"), expectedSet);
        assertTrue(config.createUsers());
        assertTrue(config.deleteUsers());
        assertTrue(config.updateUsers());
        assertTrue(config.updateVoicemails());
        assertFalse(config.deleteVoicemails());
        assertFalse(config.createVoicemails());
        assertTrue(config.deleteLines());
        assertFalse(config.createLines());
        assertFalse(config.updateLines());
        
        assertTrue(config.createAgents());
        assertTrue(config.updateAgents());
        assertFalse(config.deleteAgents());

        assertFalse(config.deleteIncalls());
        assertFalse(config.updateIncalls());
        assertFalse(config.createIncalls());
        assertTrue(config.updateLdap());
        Set<String> expectedUpdatedFields = new HashSet<String>();
        expectedUpdatedFields.add("User.mobile");
        expectedUpdatedFields.add("Line.number");
        assertEquals(expectedUpdatedFields, config.getUpdatedLdapFields());
        assertEquals("(objectClass=*)", config.get("ldapSynchronizationFilter"));
        assertEquals("/somewhere/", config.get("reportFolder"));
        assertEquals("localhost", config.get("XiVOIP"));
        assertEquals("50050", config.get("restapiPort"));
        assertTrue(config.mustReenableLiveReload());
        assertEquals("Invalid Installation Id","XiVOMain", config.installationId());
        assertTrue(config.useTls());
    }

    @Test
    public void testMissingSections() throws IOException {
        String configString = "";
        writeConnnectorFile(configString);
        Configuration config = ConfigLoader.getConfig();
        assertNotNull(config);
    }

    @Test
    public void testNoUpdateLdap() throws IOException {
        String configString = "[Actions]\n";
        writeConnnectorFile(configString);
        Configuration config = ConfigLoader.getConfig();
        assertTrue(config.getUpdatedLdapFields().isEmpty());
    }

    @Test
    public void testReportFolderWithTrailingSlash() throws IOException {
        generalString = generalString.replace("/somewhere", "/somewhere/");
        writeGeneralFile(generalString);
        writeConnnectorFile("");
        Configuration config = ConfigLoader.getConfig();
        assertEquals("/somewhere/", config.get("reportFolder"));
    }

    private void writeConnnectorFile(String configString) throws IOException {
        connectorFile = new File(connectorFileName);
        writeFile(connectorFile, configString);
    }

    private void writeGeneralFile(String configString) throws IOException {
        restapiFile = new File(restapiFileName);
        writeFile(restapiFile, configString);
    }

    private void writeFile(File file, String content) throws IOException {
        file.delete();
        File directory = new File(file.getParent());
        directory.mkdirs();
        file.createNewFile();
        FileWriter fw = new FileWriter(file);
        fw.write(content);
        fw.flush();
        fw.close();
    }

}
