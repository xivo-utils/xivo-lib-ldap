package xivo.utils;

import static org.junit.Assert.*;
import static xivo.utils.PropertyTester.propertiesNeedsUpdate;
import static xivo.utils.PropertyTester.propertyNeedsUpdate;

import org.junit.Test;

public class TestPropertyTester {

    static class Box {
        String value = "";
        public Box(String value) {
            this.value = value;
        }

        public static Box of(String value) {
            return new Box(value);
        }
    }

    static class BiBox {
        String first = "";
        String second = "";
        public BiBox(String first, String second) {
            this.first = first;
            this.second = second;
        }

        public static BiBox of(String first, String second) {
            return new BiBox(first, second);
        }
    }

    static class BoxAccessor implements PropertyTester.Accessor<Box> {
        @Override
        public String getProperty(Box a) {
            return a.value;
        }
    }

    BoxAccessor accessor = new BoxAccessor();

    @Test
    public void testNullObjectEquality() {
        Box a = null;
        Box b = null;

        assertFalse(propertyNeedsUpdate(a, b, accessor));
    }

    @Test
    public void testOneObjectIsNotNullInequality() {
        Box a = Box.of("a");
        Box b = null;

        assertTrue(propertyNeedsUpdate(a, b, accessor));
        assertTrue(propertyNeedsUpdate(b, a, accessor));
    }

    @Test
    public void testNullEquality() {
        Box a = Box.of(null);
        Box b = Box.of(null);

        assertFalse(propertyNeedsUpdate(a, b, accessor));
    }

    @Test
    public void testOneNotNullInequality() {
        Box a = Box.of("a");
        Box b = Box.of(null);

        assertTrue(propertyNeedsUpdate(a, b, accessor));
        assertTrue(propertyNeedsUpdate(b, a, accessor));
    }

    @Test
    public void testInequality() {
        Box a = Box.of("a");
        Box b = Box.of("b");

        assertTrue(propertyNeedsUpdate(a, b, accessor));
        assertTrue(propertyNeedsUpdate(b, a, accessor));
    }

    @Test
    public void testEquality() {
        Box a = Box.of("a");
        Box b = Box.of("a");

        assertFalse(propertyNeedsUpdate(a, b, accessor));
        assertFalse(propertyNeedsUpdate(b, a, accessor));
    }

    @Test
    public void testBiNullObjectEquality() {
        BiBox a = null;
        BiBox b = null;

        assertFalse(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
    }

    @Test
    public void testBiOneObjectIsNotNullInequality() {
        BiBox a = BiBox.of("first", "second");
        BiBox b = null;

        assertTrue(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
        assertTrue(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
    }

    @Test
    public void testBiNullEquality() {
        BiBox a = BiBox.of(null, null);
        BiBox b = BiBox.of(null, null);;

        assertFalse(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
        assertFalse(propertiesNeedsUpdate(b, a, x -> x.first, x -> x.second));

    }

    @Test
    public void testBiOneNotNullInequality() {
        BiBox a = BiBox.of("first", "second");
        BiBox b = BiBox.of(null, null);;

        assertTrue(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
        assertTrue(propertiesNeedsUpdate(b, a, x -> x.first, x -> x.second));
        b = BiBox.of("first", null);
        assertTrue(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
        assertTrue(propertiesNeedsUpdate(b, a, x -> x.first, x -> x.second));
        b = BiBox.of(null, "second");
        assertTrue(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
        assertTrue(propertiesNeedsUpdate(b, a, x -> x.first, x -> x.second));
    }

    @Test
    public void testBiInequality() {
        BiBox a = BiBox.of("first", "second");
        BiBox b = BiBox.of("first", "other");

        assertTrue(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
        assertTrue(propertiesNeedsUpdate(b, a, x -> x.first, x -> x.second));

        b = BiBox.of("other", "second");
        assertTrue(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));
        assertTrue(propertiesNeedsUpdate(b, a, x -> x.first, x -> x.second));

    }

    @Test
    public void testBiEquality() {
        BiBox a = BiBox.of("first", "second");
        BiBox b = BiBox.of("first", "second");

        assertFalse(propertiesNeedsUpdate(a, b, x -> x.first, x -> x.second));

    }


}
