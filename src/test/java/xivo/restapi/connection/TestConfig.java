package xivo.restapi.connection;

import java.util.HashMap;
import java.util.Map;

public class TestConfig implements RestapiConfig {

    private Map<String, String> params = new HashMap<String, String>();

    public String get(String name) {
        return params.get(name);
    }

    public void set(String name, String value) {
        params.put(name, value);
    }

}
