package xivo.restapi.connection.managers;

import com.google.gson.reflect.TypeToken;
import junit.framework.TestCase;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.model.Voicemail;
import xivo.restapi.model.Links.UserVoicemail;

import java.util.Arrays;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ VoicemailManager.class })
public class TestVoicemailManager extends TestCase {

    private RequestExecutor executor;
    private JsonSerializer serializer;
    private String voicemailsUrl;
    private VoicemailManager manager;
    private VoicemailManager managerWithRealSerializer;
    private Voicemail voicemail;
    private String response = "";
    private String serializedString = "";
    private List<UserVoicemail> voicemailLinks;
    private List<Voicemail> voicemails;

    @Before
    public void setUp() {
        executor = PowerMock.createMock(RequestExecutor.class);
        serializer = PowerMock.createMock(JsonSerializer.class);
        voicemailsUrl = "voicemails";
        manager = new VoicemailManager(executor, serializer, voicemailsUrl);
        managerWithRealSerializer = new VoicemailManager(executor, new JsonSerializer(), voicemailsUrl);
        voicemail = new Voicemail();
        voicemail.setId(4);

        UserVoicemail userVoicemail1 = new UserVoicemail();
        userVoicemail1.voicemail_id = 22;
        userVoicemail1.user_id = 101;
        UserVoicemail userVoicemail2 = new UserVoicemail();
        userVoicemail2.voicemail_id = 22;
        userVoicemail2.user_id = 250;
        voicemailLinks = Arrays.asList(userVoicemail1, userVoicemail2);
    }

    @Test
    public void testDelete() throws Exception {
        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, voicemailsUrl + "/4").andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.replayAll();

        manager.delete(voicemail);

        PowerMock.verifyAll();
    }

    @Test
    public void testCreate() throws Exception {
        String response = "{\"id\": 41,\"links\": [{\"rel\": \"voicemails\", \"href\": \"http://...\"}]}";

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, voicemailsUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(voicemail)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response);
        EasyMock.expect(serializer.extractId(response)).andReturn(41);
        PowerMock.replayAll();

        manager.create(voicemail);

        PowerMock.verifyAll();
        assertEquals(41, voicemail.getId().intValue());
    }

    @Test
    public void testUpdate() throws Exception {
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, voicemailsUrl + "/4").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(voicemail)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);

        PowerMock.replayAll();

        manager.update(voicemail);

        PowerMock.verifyAll();
    }

    @Test
    public void testGet() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, voicemailsUrl + "/2").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, Voicemail.class)).andReturn(voicemail);
        PowerMock.replayAll();

        Voicemail result = manager.get(2);

        PowerMock.verifyAll();
        assertEquals(voicemail, result);
    }

    @Test
    public void getAssociatedUsersIds() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, voicemailsUrl + "/22/users").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeList(EasyMock.eq(response), EasyMock.notNull(TypeToken.class))).andReturn(voicemailLinks);
        PowerMock.replayAll();

        List<UserVoicemail> result = manager.getAssociatedUsersIds(22);

        PowerMock.verifyAll();
        assertEquals(voicemailLinks, result);
    }

    @Test
    public void getAssociatedUsersIds2() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, voicemailsUrl + "/22/users").andReturn(getMock);
        String response = "{ \"items\": [ { \"voicemail_id\": 22, \"user_id\": 101 }, { \"voicemail_id\": 22, \"user_id\": 250 } ], \"total\": 0 }";
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        PowerMock.replayAll();

        List<UserVoicemail> result = managerWithRealSerializer.getAssociatedUsersIds(22);

        PowerMock.verifyAll();
        assertEquals(voicemailLinks.size(), result.size());
        for(int i = 0; i < result.size(); i++) {
            assertEquals(voicemailLinks.get(i).user_id, result.get(i).user_id);
            assertEquals(voicemailLinks.get(i).voicemail_id, result.get(i).voicemail_id);
        }
    }

    @Test
    public void testList() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, voicemailsUrl).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeList(EasyMock.eq(response), EasyMock.notNull(TypeToken.class))).andReturn(voicemails);
        PowerMock.replayAll();

        List<Voicemail> result = manager.list();

        PowerMock.verifyAll();
        assertEquals(voicemails, result);
    }


}
