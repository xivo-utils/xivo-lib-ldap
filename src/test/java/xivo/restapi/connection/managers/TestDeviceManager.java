package xivo.restapi.connection.managers;

import org.apache.http.client.methods.HttpGet;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.model.Device;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DeviceManager.class })
public class TestDeviceManager {

    private DeviceManager manager;
    private RequestExecutor executor;
    private JsonSerializer serializer;
    private String devicesUrl;

    @Before
    public void setUp() {
        executor = EasyMock.createMock(RequestExecutor.class);
        serializer = EasyMock.createMock(JsonSerializer.class);
        devicesUrl = "devices";
        manager = new DeviceManager(executor, serializer, devicesUrl);
    }

    @Test
    public void testSynchronize() throws Exception {
        String id = "abcdefgh123456";
        Device dev = new Device(id);
        HttpGet getMock = EasyMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, devicesUrl + "/" + id + "/synchronize").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn("");
        PowerMock.replayAll();

        manager.synchronize(dev);

        PowerMock.verifyAll();
    }

    @Test
    public void testResetAutoprov() throws Exception {
        String id = "abcdefgh123456";
        Device dev = new Device(id);
        HttpGet getMock = EasyMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, devicesUrl + "/" + id + "/autoprov").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn("");
        PowerMock.replayAll();

        manager.resetAutoprov(dev);

        PowerMock.verifyAll();
    }

}
