package xivo.restapi.connection.managers;


import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.model.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MdsManager.class })
public class TestMdsManager {

    private RequestExecutor executor;
    private JsonSerializer serializer;
    private String mdsUrl;
    private MdsManager manager;
    private String token;
    private Mds mds;
    private String response = "";

    @Before
    public void setUp() {
        executor = PowerMock.createMock(RequestExecutor.class);
        serializer = PowerMock.createMock(JsonSerializer.class);
        token = "token";
        mdsUrl = "http://localhost:50050/1.1/users";
        manager = new MdsManager(executor, serializer, mdsUrl, token);
        prepareMds();
    }

    private void prepareMds() {
        mds = new Mds();
        mds.setId(1L);
        mds.setName("mds1");
        mds.setDisplayName("Media Server 1");
        mds.setVoipId("1.1.1.1");
    }

    @Test
    public void testListMds() throws Exception {
        Mds mds1 = new Mds();
        mds1.setId(1L);
        mds1.setName("default");
        mds1.setDisplayName("MDS Main");
        mds1.setVoipId("10.0.1.10");

        Mds mds2 = new Mds();
        mds2.setId(2L);
        mds2.setName("mds1");
        mds2.setDisplayName("MDS 1");
        mds2.setVoipId("10.0.1.20");

        response = "[{\"id\":1,\"name\":\"default\",\"displayName\":\"MDS Main\",\"voipIp\":\"10.0.1.10\"},{\"id\":2,\"name\":\"mds1\",\"displayName\":\"MDS 1\",\"voipIp\":\"10.0.1.20\"}]";

        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        getMock.setHeader("X-Auth-Token", token);

        PowerMock.expectNew(HttpGet.class, mdsUrl).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);

        EasyMock.expect(serializer.deserializeMds(EasyMock.eq(response)))
                .andReturn(Arrays.asList(mds1, mds2));

        PowerMock.replayAll();

        List<Mds> mdsList = manager.list();

        PowerMock.verifyAll();
        assertSame(1L, mdsList.get(0).getId());
        assertEquals("default", mdsList.get(0).getName());
        assertEquals("MDS Main", mdsList.get(0).getDisplayName());
        assertEquals("10.0.1.10", mdsList.get(0).getVoipIp());

        assertSame(2L, mdsList.get(1).getId());
        assertEquals("mds1", mdsList.get(1).getName());
        assertEquals("MDS 1", mdsList.get(1).getDisplayName());
        assertEquals("10.0.1.20", mdsList.get(1).getVoipIp());
    }
}
