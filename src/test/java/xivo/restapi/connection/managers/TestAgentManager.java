package xivo.restapi.connection.managers;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import org.powermock.reflect.Whitebox;
import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.*;
import xivo.restapi.model.Queue;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AgentManager.class })
public class TestAgentManager {

    private RequestExecutor executor;
    private JsonSerializer serializer;
    private AgentManager manager;
    private String agentsUrl;
    private String queueUrl;
    private String token;
    private String membershipsUrl;
    private String agentGroupsUrl;

    private String customIncall = "from-extern";

    private Queue qCars, qBikes;
    private AgentGroup groupDefault, groupExperts;

    @Before
    public void setUp() {
        executor = PowerMock.createMock(RequestExecutor.class);
        serializer = new JsonSerializer();
        agentsUrl = "/1.1/agents";
        queueUrl = "/api/1.0/queues";
        membershipsUrl = "/1.1/queues";
        agentGroupsUrl = "/api/1.0/queues";
        token="mysecret";
        qCars = new Queue(1, "cars", "Cars queue");
        qBikes = new Queue(2, "bikes", "Bikes queue");

        groupDefault = new AgentGroup(1, "default");
        groupExperts = new AgentGroup(3, "experts");

        manager = new AgentManager(executor, serializer, agentsUrl, queueUrl, token, membershipsUrl, agentGroupsUrl);
    }

    private void loadQueuesCache() {
        List<Queue> queues = new ArrayList<>();
        queues.add(qCars);
        queues.add(qBikes);
        Whitebox.setInternalState(manager, "_queuesCache", queues);
    }

    private void loadGroupsCache() {
        List<AgentGroup> groups = new ArrayList<>();
        groups.add(groupDefault);
        groups.add(groupExperts);
        Whitebox.setInternalState(manager, "_groupsCache", groups);
    }

    @Test
    public void testCreate() throws Exception {
        String response = "{\"context\": \"default\",\"description\": \"\",\"firstname\": \"James\",\"id\": 73,\"language\": \"fr_FR\",\"lastname\": \"Bond\",\"number\": \"007\",\"numgroup\": 1,\"passwd\": \"\"\r\n}";
        String serializedString = "{\"firstname\":\"James\",\"lastname\":\"Bond\",\"number\":\"007\"}";

        Agent agent = new Agent();
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("007");

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, agentsUrl).andReturn(postMock);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response);
        PowerMock.replayAll();

        manager.create(agent);

        PowerMock.verifyAll();

        assertThat(agent.getId(), equalTo(73));
    }

    @Test(expected = WebServicesException.class)
    public void testDoNotCreateIfNumberIsEmpty() throws Exception {

        Agent agent = new Agent();
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("");

        manager.create(agent);
    }


    @Test
    public void testCreateWithDefaultQueueByName() throws Exception {
        String response = "{\"context\": \"default\",\"description\": \"\",\"firstname\": \"James\",\"id\": 73,\"language\": \"fr_FR\",\"lastname\": \"Bond\",\"number\": \"007\",\"numgroup\": 1,\"passwd\": \"\"\r\n}";
        String userString = "{\"firstname\":\"James\",\"lastname\":\"Bond\",\"number\":\"007\"}";
        String membershipString = "{\"agent_id\":73,\"penalty\":0,\"queue_id\":" + qBikes.getId() + "}";

        Agent agent = new Agent();
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("007");
        agent.setQueuename(qBikes.getName());

        loadQueuesCache();

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, agentsUrl).andReturn(postMock);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, userString)).andReturn(response);

        HttpPost postMembership = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, membershipsUrl + "/" +  qBikes.getId() + "/members/agents").andReturn(postMembership);
        EasyMock.expect(executor.fillAndExecuteRequest(postMembership, membershipString)).andReturn("");

        PowerMock.replayAll();

        manager.create(agent);

        PowerMock.verifyAll();

        assertThat(agent.getId(), equalTo(73));
        assertThat(agent.getMemberships().size(), equalTo(1));
    }

    @Test
    public void testCreateWithGroupByName() throws Exception {
        String response = "{\"context\": \"default\",\"description\": \"\",\"firstname\": \"James\",\"id\": 75,\"language\": \"fr_FR\",\"lastname\": \"Bond\",\"number\": \"007\",\"numgroup\": 3,\"passwd\": \"\"\r\n}";
        String serializedString = "{\"firstname\":\"James\",\"lastname\":\"Bond\",\"number\":\"007\",\"numgroup\":3}";

        Agent agent = new Agent();
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("007");
        agent.setGroupname("experts");

        loadGroupsCache();

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, agentsUrl).andReturn(postMock);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response);
        PowerMock.replayAll();

        manager.create(agent);

        PowerMock.verifyAll();

        assertThat(agent.getId(), equalTo(75));
        assertThat(agent.getNumgroup(), equalTo(3));
    }

    @Test
    public void testUpdate() throws Exception {
        String payload = "{\"id\":73,\"firstname\":\"James\",\"lastname\":\"Bond\",\"number\":\"007\",\"numgroup\":1,\"context\":\"default\",\"language\":null,\"description\":null,\"passwd\":null}";

        Agent agent = new Agent();
        agent.setId(73);
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("007");
        agent.setContext("default");
        agent.setNumgroup(1);

        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, agentsUrl + "/" + agent.getId()).andReturn(putMock);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, payload)).andReturn("");
        PowerMock.replayAll();

        manager.update(agent);

        PowerMock.verifyAll();

    }

    @Test
    public void testUpdateWithDefaultQueueByName() throws Exception {
        String response = "{\"context\": \"default\",\"description\": \"\",\"firstname\": \"James\",\"id\": 73,\"language\": \"fr_FR\",\"lastname\": \"Bond\",\"number\": \"007\",\"numgroup\": 1,\"passwd\": \"\"\r\n}";
        String userString = "{\"id\":73,\"firstname\":\"James\",\"lastname\":\"Bond\",\"number\":\"007\",\"numgroup\":1,\"context\":\"default\",\"language\":null,\"description\":null,\"passwd\":null}";
        String membershipString = "{\"agent_id\":73,\"penalty\":0,\"queue_id\":" + qBikes.getId() + "}";

        Agent agent = new Agent();
        agent.setId(73);
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("007");
        agent.setContext("default");
        agent.setNumgroup(1);
        agent.setQueuename(qBikes.getName());
        loadQueuesCache();

        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, agentsUrl + "/" + agent.getId()).andReturn(putMock);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, userString)).andReturn(response);

        HttpPost postMembership = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, membershipsUrl + "/" +  qBikes.getId() + "/members/agents").andReturn(postMembership);
        EasyMock.expect(executor.fillAndExecuteRequest(postMembership, membershipString)).andReturn("");

        PowerMock.replayAll();

        manager.update(agent);

        PowerMock.verifyAll();

        assertThat(agent.getId(), equalTo(73));
        assertThat(agent.getMemberships().size(), equalTo(1));
    }

    @Test
    public void testUpdatePenaltyWithDefaultQueueByNameDoesNothing() throws Exception {
        // Here we have a user with a membership with a non-default penalty.
        // In this case, we ignore the membership update

        String response = "{\"context\": \"default\",\"description\": \"\",\"firstname\": \"James\",\"id\": 73,\"language\": \"fr_FR\",\"lastname\": \"Bond\",\"number\": \"007\",\"numgroup\": 1,\"passwd\": \"\"\r\n}";
        String userString = "{\"id\":73,\"firstname\":\"James\",\"lastname\":\"Bond\",\"number\":\"007\",\"numgroup\":1,\"context\":\"default\",\"language\":null,\"description\":null,\"passwd\":null}";
        String membershipString = "{\"agent_id\":73,\"penalty\":0,\"queue_id\":" + qBikes.getId() + "}";

        QueueMembership existingMembership = new QueueMembership(73, 3, qBikes.getId());

        Agent agent = new Agent();
        agent.setId(73);
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("007");
        agent.setContext("default");
        agent.setNumgroup(1);
        agent.initMemberships(Stream.of(existingMembership).collect(Collectors.toList()));
        agent.setQueuename(qBikes.getName());
        loadQueuesCache();

        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, agentsUrl + "/" + agent.getId()).andReturn(putMock);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, userString)).andReturn(response);

        PowerMock.replayAll();

        manager.update(agent);

        PowerMock.verifyAll();

        assertThat(agent.getId(), equalTo(73));
        assertThat(agent.getMemberships().size(), equalTo(1));
    }


    @Test(expected = WebServicesException.class)
    public void testDoNotUpdateIfNumberIsEmpty() throws Exception {

        Agent agent = new Agent();
        agent.setId(73);
        agent.setFirstname("James");
        agent.setLastname("Bond");
        agent.setNumber("");
        agent.setContext("default");
        agent.setNumgroup(1);

        manager.update(agent);
    }

    @Test
    public void testDelete() throws Exception {
        Agent agent = new Agent();
        agent.setId(73);

        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, agentsUrl + "/" + agent.getId()).andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn("");
        PowerMock.replayAll();

        manager.delete(agent);

        PowerMock.verifyAll();

    }

    @Test
    public void testQueueCache() throws Exception {
        String response = "[{\"id\": 8, \"name\": \"planes\", \"displayName\": \"Planes\"}, {\"id\": 11, \"name\": \"trucks\", \"displayName\": \"Trucks\"}]";
        Queue qPlanes = new Queue(8, "planes", "Planes");
        Queue qTrucks = new Queue(11, "trucks", "Trucks");


        HttpGet request = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, queueUrl).andReturn(request);
        EasyMock.expect(executor.executeRequest(request)).andReturn(response);
        PowerMock.replayAll();

        List<Queue> result = manager.listQueuesCache();

        PowerMock.verifyAll();

        assertThat(result, hasItems(qPlanes, qTrucks));
    }

    @Test
    public void testGetQueueById() throws Exception {
        loadQueuesCache();
        assertThat(manager.getQueueById(qCars.getId()), equalTo(qCars));
        assertThat(manager.getQueueById(qBikes.getId()), equalTo(qBikes));
    }

    @Test
    public void testGetQueueByName() throws Exception {
        loadQueuesCache();
        assertThat(manager.getQueueByName(qCars.getName()), equalTo(qCars));
        assertThat(manager.getQueueByName(qBikes.getName()), equalTo(qBikes));
    }

    @Test
    public void testGetQueueByNameIgnoreCase() throws Exception {
        loadQueuesCache();
        assertThat(manager.getQueueByName(qCars.getName().toUpperCase()), equalTo(qCars));
        assertThat(manager.getQueueByName(qBikes.getName().toUpperCase()), equalTo(qBikes));
    }

    @Test
    public void testGroupsCache() throws Exception {
        String response = "[{\"id\": 1, \"name\": \"default\"}, {\"id\": 3, \"name\": \"experts\"}]";
        AgentGroup g1= new AgentGroup(1, "default");
        AgentGroup g3= new AgentGroup(3, "experts");

        HttpGet request = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, agentGroupsUrl).andReturn(request);
        EasyMock.expect(executor.executeRequest(request)).andReturn(response);
        PowerMock.replayAll();

        List<AgentGroup> result = manager.listGroupsCache();

        PowerMock.verifyAll();

        assertThat(result, hasItems(g1, g3));
    }

    @Test
    public void testGetGroupById() throws Exception {
        loadGroupsCache();
        assertThat(manager.getGroupById(groupDefault.getId()), equalTo(groupDefault));
        assertThat(manager.getGroupById(groupExperts.getId()), equalTo(groupExperts));
    }

    @Test
    public void testGetGroupByName() throws Exception {
        loadGroupsCache();
        assertThat(manager.getGroupByName(groupDefault.getName()), equalTo(groupDefault));
        assertThat(manager.getGroupByName(groupExperts.getName()), equalTo(groupExperts));
    }

    @Test
    public void testGetGroupByNameIgnoreCase() throws Exception {
        loadGroupsCache();
        assertThat(manager.getGroupByName(groupDefault.getName().toUpperCase()), equalTo(groupDefault));
        assertThat(manager.getGroupByName(groupExperts.getName().toUpperCase()), equalTo(groupExperts));
    }

}
