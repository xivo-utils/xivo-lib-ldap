package xivo.restapi.connection;

import junit.framework.TestCase;

import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;

public class TestHttpClientFactory extends TestCase {

    private HttpClientFactory factory;

    @Test
    public void testCreateHttpClientLocalhost() throws Exception {
        TestConfig config = new TestConfig();
        config.set(RestapiConfig.RESTAPI_URL, "http://localhost:50050");
        factory = new HttpClientFactory(config);
        DefaultHttpClient client = factory.get();
        assertEquals(client.getConnectionManager().getSchemeRegistry().get("https").getDefaultPort(), 443);
    }

    @Test
    public void testCreateHttpClientOtherHost() throws Exception {
        TestConfig config = new TestConfig();
        config.set(RestapiConfig.RESTAPI_URL, "https://localhost:50051");
        factory = new HttpClientFactory(config);
        DefaultHttpClient client = factory.get();
        assertEquals(client.getConnectionManager().getSchemeRegistry().get("https").getDefaultPort(), 50051);
    }

}
