package xivo.restapi.model;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestAgent {

    @Test
    public void testAddMembership() {
        QueueMembership m1 = new QueueMembership(1, 0, 1);
        QueueMembership m2 = new QueueMembership(1, 3, 2);
        Agent a = new Agent();
        a.setId(1);
        a.setMembership(m1.getQueue_id(), m1.getPenalty());
        a.setMembership(m2.getQueue_id(), m2.getPenalty());
        Set<QueueMembership> ms = a.getMembershipsToAdd();
        assertThat(ms, hasItems(m1, m2));
    }


    @Test
    public void testUpdateMembership() {
        QueueMembership m1 = new QueueMembership(1, 0, 1);
        QueueMembership m1Updated = new QueueMembership(1, 5, 1);
        QueueMembership m2 = new QueueMembership(1, 3, 2);
        Agent a = new Agent();
        a.setId(1);
        a.initMemberships(Stream.of(m1, m2).collect(Collectors.toList()));
        a.setMembership(m1Updated.getQueue_id(), m1Updated.getPenalty());
        Set<QueueMembership> ms = a.getMembershipsToUpdate();
        assertThat(ms, hasItems(m1Updated));
    }

    @Test
    public void testDeleteMembership() {
        QueueMembership m1 = new QueueMembership(1, 0, 1);
        QueueMembership m2 = new QueueMembership(1, 3, 2);
        QueueMembership m3 = new QueueMembership(1, 3, 3);
        Agent a = new Agent();
        a.setId(1);
        a.initMemberships(Stream.of(m1, m2, m3).collect(Collectors.toList()));
        a.removeMembership(2);
        a.removeMembership(3);
        Set<Integer> ms = a.getMembershipsToDelete();
        assertThat(ms, hasItems(2, 3));
    }

    @Test
    public void testDeleteAllMembership() {
        QueueMembership m1 = new QueueMembership(1, 0, 1);
        QueueMembership m2 = new QueueMembership(1, 3, 2);
        QueueMembership m3 = new QueueMembership(1, 3, 5);
        Agent a = new Agent();
        a.setId(1);
        a.initMemberships(Stream.of(m1, m2, m3).collect(Collectors.toList()));
        a.removeAllMembership();
        Set<Integer> ms = a.getMembershipsToDelete();
        assertThat(ms, hasItems(m1.getQueue_id(), m2.getQueue_id(), m3.getQueue_id()));
    }

}
