package xivo.restapi.model;

import java.util.Arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestContext {

    @Test
    public void testIsNumberInbound() {
        ContextInterval it1 = new ContextInterval("100", "199");
        ContextInterval it2 = new ContextInterval("400", "499");
        Context context = new Context("from-extern", Arrays.asList(it1, it2));

        assertTrue(context.isNumberInbound("110"));
        assertTrue(context.isNumberInbound("400"));
        assertFalse(context.isNumberInbound("99"));
        assertFalse(context.isNumberInbound("201"));
        assertFalse(context.isNumberInbound("503"));
    }

    @Test
    public void testIsNumberInbound_openInterval() {
        ContextInterval it1 = new ContextInterval("100", "199");
        ContextInterval it2 = new ContextInterval("400", null);
        Context context = new Context("from-extern", Arrays.asList(it1, it2));

        assertTrue(context.isNumberInbound("410"));
    }

    @Test
    public void testGetIntervalForNumber() {
        ContextInterval it1 = new ContextInterval("100", "199");
        ContextInterval it2 = new ContextInterval("5400", null, 3);
        Context context = new Context("from-extern", Arrays.asList(it1, it2));

        assertSame(it1, context.getIntervalForNumber("105"));
        assertSame(it2, context.getIntervalForNumber("5454"));
        assertNull(context.getIntervalForNumber("203"));
    }
}
