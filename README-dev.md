Développement d'un connecteur LDAP
==================================

xivo-lib-ldap contient le code de base permettant de développer un connecteur LDAP. Il utilise le framework d'injection de dépendances Google Guice.
Son principe de fonctionnement est le suivant : 
* à chaque entrée du LDAP, est associé un et un seul utilisateur de XiVO. Un identifiant de corrélation, généré à partir des données du LDAP, est stocké dans le champ *description* de XiVO.
* cet identifiant est généré lors de la création de l'utilisateur dans XiVO
* si des utilisateurs ont été créés dans XiVO avant la mise en place du connecteur LDAP, il est possible de procéder à une initialisation, c'est-à-dire d'injecter cet identifiant de corrélation dans les utilisateurs préexistant dans XiVO.

Un connecteur spécifique peut ainsi être développé en quelques heures en suivant les étapes suivantes :

1) étendre les classes / réaliser les interfaces de xivo-lib-ldap
2) créer un main()
3) écrire les fichiers de configuration

1) implémentation des classes abstraites / interfaces de xivo-lib-ldap
----------------------------------------------------------------------

* implémenter les méthodes setUp() et performSpecificActions() de AbstractLdapSynchronizer; les laisser vides s'il n'y a rien de spécial à faire; elles seront appelées respectivement avant et après la synchronisation.

* implémenter les méthodes xivoNeedsUpdate() et ldapNeedsUpdate() de AbstractDeltaGenerator : elles doivent renvoyer *true* si respectivement le XiVO ou le LDAP nécessite une mise à jour pour l'utilisateur donné.

* implémenter l'interface Initializer : elle est responsable d'une éventuelle phase d'initialisation. Si aucune initialisation n'est nécessaire, implémenter tout de même l'interface (on peut par exemple jeter une exception UnsupportedOperation)

* implémenter l'interface UserModifier : 
1, méthode modifyXivoUser est utilisée dans la désérialisation des données du LDAP; elle permet de modifier des attributs de  l'utilisateur généré à partir des données du LDAP (utile notamment si un atribut de l'utilisateur est constitué de plusieurs attributs du LDAP)
2, méthode modifyLdapUser est appelée avant la mise à jour des données dans LDAP; elle permet de modifier des attributs de l'utilisateur à insérer dans LDAP (utile pour adapter les données au format utilisé par LDAP, par exemple le format des numéros)

* si une mise à jour doit avoir lieu dans le sens XiVO -> LDAP, il faut implémenter UserIdentifier : la méthode getSearchCriteria() doit renvoyer une requête de recherche dans le LDAP permettant de retrouver l'UNIQUE entrée associée à l'utilisateur. Exemple de retour : "(uid=123abc)"
Il n'est pas nécessaire de l'implémenter si aucune mise à jour du LDAP n'est effectuée

* enfin, implémenter la méthode specialBinds() de AbstractLDAPModule : elle permet d'indiquer à Google Guice quelles implémentations utiliser pour les classes et interfaces citées ci-dessus. Exemple : *bind(UserModifier.class).to(UserModifierImpl.class);*
Attention : cette classe concrète doit définir un constructeur prenant un booléen en paramètre et appelent *super(bool)*.

2) écrire un main
-----------------

Le main doit simplement appeler la méthode AbstractLdapSynchronizer.start() avec la classe implémentant AbstractLDAPModule et les paramètres de ligne de commande :

 *AbstractLdapSynchronizer.start(ConcreteLDAPModule.class, args);*

3) écrire les fichiers de configuration
---------------------------------------

Reprendre tels quels les fichiers general.ini et logging.properties d'un projet existant. Le fichier config.ini doit être adapté au connecteur.



Tests d'intégration
===================

* Un OpenLDAP de test peut être monté par le docker (noms et domain à adapter):
docker run --name client-xy-ldap --env LDAP_ORGANISATION="Client XY" --env LDAP_DOMAIN="ldap.example.org" --detach -p 389:389 osixia/openldap:1.1.1

* ensuite il faut créer la structure, voici un exemple pour créer un ou=People:
- dans le fichier ldap-init.ldif:
 dn: ou=People,dc=ldap,dc=example,dc=org
 ou: People
 description: All people in ldap.example.org
 objectClass: top
 objectClass: organizationalUnit
- initier LDAP par
ldapadd -H ldap://@IPduDockerContainer -x -D "cn=admin,dc=ldap,dc=example,dc=org" -f ldap-init.ldif -w admin

* et après intégrer l'export de ldap du client (au format ldif récupéré par ldapseqrch d'apres les instructions dans le readme)
ldapadd -H ldap://@IPduDockerContainer -x -D "cn=admin,dc=ldap,dc=example,dc=org" -f ldap.ldif -w admin
- si le client utilise des class non standards, le plus simple est de filter les lignes s'y référantes (par ex. par cat ldap.ldif | grep -v 'uidNumber' > ldap1.ldif)

* Pour un test rapide on peut utiliser le ldiff minimal suivant:
dn: CN=gesnaud,ou=People,dc=ldap,dc=example,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetorgPerson
cn: gesnaud
uid: gesnaud
mail: gesnaud@avencall.com
telephoneNumber: 44206
givenName: Grégory
sn: ESNAUD
displayName: Esnaud Grégory

Notes:
======

### Xivo context is always default

See : https://projects.xivo.solutions/issues/167

The context field of XiVO in config.ini is not used by the lib-ldap.

The default context is the value defined in Line.java.

I think it's because in createUser of LDAPDeserializer we base xivo creation from ldapfield. But, nowhere in fieldMapping we are talking about context... it is specified in [XiVO].

So I think that when instantiating a ldapsycnhro, we should replace the default value in LINE.java with the one in config.ini.

A workaround is to use userNeedsCreation() with

        protected boolean needsCreation(User ldapUser) {
            Configuration config = ConfigLoader.getConfig();
            ldapUser.getLine().setContext(config.get("context"));
            return true;
        }

### Using binary LDAP format 

See : https://projects.xivo.solutions/issues/166

xivo-lib-ldap is not aware of handling octet string attribute, especially ObjectGUID when getting ldap data (at beginning start of the synchronization).

One of our client would use ObjectGUID as a unique key for ldapSynchro, this sounds like a good idea.
But, because of the ObjectGUID format (http://www.developerscrappad.com/1109/windows/active-directory/java-ldap-jndi-2-ways-of-decoding-and-using-the-objectguid-from-windows-active-directory/), our library cannot deal with it...

The issue is at line 68 in LDAPSerializer.java (xivo.ldap.connection) in createUser Class:

String attrValue = (String) attribute.get();

So if I had one attribute in Octet string format, I have a cast error.

A workaround is to comment in config.ini the attribute in error. As an example:

*;*User.description=objectGUID

And then, implement UserModifierImpl() with a call to getADHexString() to cast HexString desired attribute (the one commented above) into String to inject it in XiVO at last:

    public User modifyXivoUser(User user, SearchResult searchResult) throws NamingException {
    
        String description = decoder.getADHexString((byte[]) searchResult.getAttributes().get("objectGUID").get());
        user.setDescription(description);
        return user;
    }

I would like getADHexString() accessible from LDAPSynchronizer, in Setup().
I think it could be ok as ldapConnector object is created before. But, in ldapConnectorImpl, the LDAPDeSerializer is a private attribute and createUser is not abstract.